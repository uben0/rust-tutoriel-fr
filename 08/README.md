# Entré de texte

```rust
println!("Entrez votre âge:");
let age: u32 = user_input_u32();
println!("Votre âge est {}", age);
```
On imagine ici que la fonction `user_input_u32` récupère ce qui est tapé dans le terminal pour l'interpréter comme un entier et le retourner.
Malheuresement, il n'y a pas de telle fonction fournie de base, c'est pourquoi je vais en fournir une :

```rust
fn user_input<T: std::str::FromStr>() -> T
where <T as std::str::FromStr>::Err: std::fmt::Debug
{
	let mut l = String::new();
	std::io::stdin().read_line(&mut l).unwrap();
	T::from_str(l.as_str().trim()).unwrap()
}
```
Nous n'essayerons pas de comprendre ce que fait cette fonction, car ici, beaucoup de notions avancés sont utilisées et ce d'une façon peu élégante. Elle est juste là pour faciliter les entrés de texte dans ce tutoriel.

Exemple de son utilisation:
```rust
println!("En quelle année es-tu né ?");
let annee_naissance: i32 = user_input();

println!("Quel est ton nom ?");
let nom: String = user_input();

println!(
	"Tu es né en {} et tu t'appelles {}.",
	annee_naissance, nom
);
```
Le type utilisé pour l'année est `i32` et celui pour le nom est `String`. Cependant, dans les deux cas, `user_input()` a été appelée, hors une fonction ne peut retourner qu'un type bien défini, quelle est donc cette magie noir ? Comme je l'ai dis, la déclaration de la fonction `user_input` fait appelle à des mécaniques que nous ne verons que plus tard.