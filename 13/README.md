# Boucle for

Il existe une boucle `for` en Rust, mais je ne l'ai pas présentée avant car elle fait appelle à des mécaniques plus avancées, cepedant, on peut l'utiliser assez facilement sans avoir à comprendre comment elle fonctionne vraiment.
```rust
for i in 0..10 {
	println!("{}", i);
}
```
Cette boucle affiche les entiers de `0` à `9` inclu.

## Variable
```rust
println!("Entrez un entier positif :");
let n: u32 = user_input();
for i in 0..n {
	println!("{}", i);
}
```
On peut utiliser une variable pour spécifier l'interval.

## Boucles imbriquées

Il est tout à fait possible d'imbriquer des boucles :
```rust
let width = 46;
let height = 32;

for y in 0..height {
	for x in 0..width {
		if (x + y) % 2 == 0 {
			print!("  ");
		}
		else {
			print!("██");
		}
	}
	println!("");
}
```
Ce code affiche un damier dans le terminal.

## Iterator

La boucle `for` parcours des `iterator`s, on vera que beaucoup de types les utilises, par exemple les tableaux. Il est possible de parcourir un tableau:
```rust
let t = [3, 5, 1, 8, 3, 2];

for i in &t {
	println!("{}", i);
}
```
Cependant, il y a trop de choses à expliquer en une seule fois. Par exemple, pourquoi on a `&t` et pas juste `t`. Pourquoi `i` est de type `&i32` et pas juste `i32`.
