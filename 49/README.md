# Jeu d'échecs (9)

Nous allons faire en sorte que notre programme détecte quand un joueur est en échecs et mat ou en pat.

Dans un premiers temps, nous allons créer une fonction `possible_piece_moves` qui indique une liste de déplacements possibles de cette pièce, c'est à dire les cous jouables.

```rust
fn possible_piece_moves(&self, x: usize, y: usize, moves: &mut Vec<Move>) {
    // on vérifie qu'il y est bien une pièce à l'emplacement
    if let Some(piece) = self.board[y][x] {
        let mut map = [[false; 8]; 8];
        self.reachable_by_piece(x, y, &mut map);
        for dst_y in 0..8 {
            for dst_x in 0..8 {
                // pour toutes les cases accessibles par cette pièce
                if map[dst_y][dst_x] {
                    let mut predict = self.clone();
                    let m = Move{src_x: x, src_y: y, dst_x, dst_y};
                    // tester le déplacement
                    predict.move_piece(m);
                    // si ça ne met pas le joueur en échec
                    if !predict.is_in_check(piece.c) {
                        // alors le déplacement est possible
                        moves.push(m);
                    }
                }
            }
        }
    }
}
```

Créons une fonction `possible_player_moves` qui indique une liste de déplacements possibles jouables par le joueur.
```rust
fn possible_player_moves(&self, player: Color, moves: &mut Vec<Move>) {
    for (x, y, piece) in self.pieces() {
        if piece.c == player {
            self.possible_piece_moves(x, y, moves);
        }
    }
}
```

Puis modifions la fonction `main` pour qu'elle test si un joeur est en échec, en échec et mat ou pat.
```rust
fn main() {
    let mut game = Game::new();
    let mut possible_moves = Vec::new();

	for &player in [White, Black].iter().cycle() {
		game.print();
        println!("{:?} player", player);
        
        // on récupère les différents cous possibles
        game.possible_player_moves(player, &mut possible_moves);
        match (game.is_in_check(player), possible_moves.is_empty()) {
            // si le joueur est en échec et qu'il ne peut jouer aucun
            // cou, alors il est en échec et mat
            ( true,  true) => {println!("CHECKMATE!"); break},
            // si le joueur est en échec mais qu'il peut jouer un cou,
            // alors il est juste en échec
            ( true, false) => println!("IN CHECK!"),
            // si le joueur n'est pas en échec mais qu'il ne peut jouer
            // aucun cou, alors il est pat
            (false,  true) => {println!("STALEMATE!"); break},
            (false, false) => (),
        }
        possible_moves.clear();

		loop {
			let m = get_user_move_loop();
			if game.play_move(m, player) {
				break
			}
			println!("this is not a valid move");
		}
    }
}
```

## Progression

Retrouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/49/main.rs">`main.rs`</a> le projet.
