# Types

En Rust, les types sont très importants, et des règles strictes les régissent. Il y a les types primitifs (définits dans le language) comme les entiers, les booléens, les charactères, les tableaux, les tuples, les références, les fonctions, etc... Et d'autres type qui sont eux, déclarés, comme les structures et les énumération.

## Types primitifs

Entiers
-------

| type | bits | min val | max val |
|------|-----:|--------:|--------:|
|`u8` | 8  | 0| 255|
|`u16`| 16 | 0| 65 535|
|`u32`| 32 | 0| 4 294 967 296|
|`u64`| 64 | 0| ...|
|`usize`| ? | 0| ...|
|`i8` | 8  | -128| 127|
|`i16`| 16 | -32768| 32767|
|`i32`| 32 | -2147483648| 2147483647|
|`i64`| 64 | ...| ...|
|`isize`| ? | ...| ...|

Les types `usize` et `isize` ont une taille en bit qui dépend de l'architecture du processeur, c'est à dire `32` bits pour un CPU 32 bits et `64` pour un CPU 64 bits.

Décimaux
--------

| type | bits | min val | max val | exemple |
|------|-----:|--------:|--------:|--:|
|`f32` | 32  | -34028235 · 10^31| 34028235 · 10^31| `-0.00985` |
|`f64`| 64 | -17976931348623157 · 10^292| 17976931348623157 · 10^292

Autres
------

| type | bits | exemple 1 | exemple 2 |
|------|-----:|--------:|--------:|
|`bool` | 8  | `true`| `false`|
|`char` | 32  | `'a'`| `'あ'`|

Le type `char` est codé sur 32 bits, ce n'est pas un octet comme en *C*. Cela permet de représenter tout les charactères unicode !

Tableaux
--------

| type | bits | exmple |
|------|-----:|--------:|
|`[u32; 4]` | 32 × 4  | `[8, 3, 5, 1]` |
|`[bool; 3]`| 8 × 3 | `[true, true, false]`|
Un tableau peut être de toutes tailles et de tous types.

Tuples
------

| type | bits | exemple |
|------|-----:|--------:|
|`()` | 0 | `()`|
|`(i32,)` | 32 | `(10,)`|
|`(i32, bool)` | 32 + 8  | `(8, true)`|
|`(char, bool, u16)` | 32 + 8 + 16  | `('a', true, 34)`|
Les tuples sont un groupement. Ils peuvent contenir zero, une ou plusieurs valeurs.


## Conversion

Il n'y a pas de conversion implicite en Rust d'un type vers un autre, par exemple:
```rust
let a: i32 = 0;
let b: u32 = a; // ERROR
```
On essaye d'affecter une valeur de type `i32` à une variable de type `u32`.
La solution est d'utiliser le mot clé `as`:
```rust
let a: i32 = 0;
let b: u32 = a as u32; // OK
```
Si il n'y a pas de conversion implicite, c'est parce que Rust priorétise la fiabilité du code, à titre d'exemple, c'est une conversion entre un flotant 64 bits vers un entier 16 bits qui a causé la destruction d'une ariane 5 lors de son décolage.

## Opérations
```rust
let a: i32 = 42;
let b: i32 = a * 3 + 100;
let c: i32 = (b - 30) / 2;
let d: bool = c % 2 == 0;
let e: bool = b < 250 && d == false;
let f: bool = !e || b != a;
```
Les opérations fonctionnent comme en *C*, si ce n'est qu'elles sont sensibles au type. En effet, il n'est possible par exemple de ne multiplier que deux valeurs du même types:
```rust
let a: u32 = 42;
let b: i16 = -3;
let b: i16 = a * b; // ERROR
```
Il faut convertir une des valeurs
```rust
let a: u32 = 42;
let b: i16 = -3;
let b: i16 = (a as i16) * b;
```
C'est valable pour toutes les opérations binaires `+`, `-`, `*` et `/`.

## Chaînes de caractères

Il y a deux types pour les chaînes de caractères: `String` et `&str`, le premier est une structure, l'autre une référence. J'expliquerais la différence entre `String` et `&str` plus tard.