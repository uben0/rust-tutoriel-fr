# Les références

Il n'y a pas de pointeurs à proprement parler en *Rust* mais des **références**. Une référence est techniquement un pointeur mais avec des règles strictes. Une référence est garantie de toujours être valide. On ne peut pas faire d'erreur en utilisant une référence.

```rust
let a: i32 = 4;
let b: &i32 = &a;

println!("a: {}, b: {}", a, *b);
```
```
a: 4, b: 4
```
La variable `b` est une référence vers `a`, on peut voir qu'il s'agit d'une référence grâce à son type noté `&i32` car `i32` est précédé d'un `&`. La variable `b` contient l'adresse mémoire de la variable `a`.

## Déréférer
```rust
let a: i32 = 4;
let b: &i32 = &a;

let c: i32 = *b;
```
On utilise l'opérateur `*` pour accéder à la valeur derrière une référence. Cependant, dans beaucoup de cas, ce n'est pas necessaire, la déréférence se fait automatiquement. Le mieux quand on débute est de ne déréférencer que quand le compilateur se plein qu'on ne l'a pas fait.

## Mutabilité par référence

Imaginons que je veuille déplacer un `Point` d'un delta `x` et `y`, alors je peut écrire une fonction :
```rust
fn deplace_point(p: Point, dx: f64, dy: f64) {
	p.x += dx; // ERROR
	p.y += dy;
}
```
Oups... On a oublié de mettre `p` mutable, corrigeons ça :
```rust
fn deplace_point(mut p: Point, dx: f64, dy: f64) {
	p.x += dx; // OK
	p.y += dy; // OK
}
```
Seulement voilà, si j'utilise ma fonction, elle semble n'avoir aucun effet :
```rust
let p: Point = Point { x: 10.0, y: 7.0 };
println!("{:?}", p);
deplace_point(p, 5.0, 8.0);
println!("{:?}", p);
```
```
Point { x: 10.0, y: 7.0 }
Point { x: 10.0, y: 7.0 }
```
En appelant `deplace_point` on a copié `p`, donc dans `deplace_point` on modifie une variable **locale**, elle apartient à `deplace_point`, elle cesse d'exister quand `deplace_point` se termine. C'est exactement comme si on avait incrémenté un entier :
```rust
fn incremente(mut x: i32) {
	x += 1;
}
```
```rust
let a = 4;
incremente(a);
println!("{}", a);
```
```
4
```

Une des solutions serait de retourner la nouvelle valeur :
```rust
fn deplace_point(mut p: Point, dx: f64, dy: f64) -> Point {
	p.x += dx;
	p.y += dy;
	return p;
}
```
```rust
let mut p: Point = Point { x: 10.0, y: 7.0 };
println!("{:?}", p);

p = deplace_point(p, 5.0, 8.0);
println!("{:?}", p);
```
```
Point { x: 10.0, y: 7.0 }
Point { x: 15.0, y: 15.0 }
```
Ça fonctionne mais ce n'est pas pratique, et puis il y a un défaut, imaginons que `Point` soit une structure volumineuse, c'est à dire avec beaucoup d'atributs, on copierai beaucoup de donnés pour rien.

La solution est de prendre une référence mutable:
```rust
fn deplace_point(p: &mut Point, dx: f64, dy: f64) {
	p.x += dx;
	p.y += dy;
}
```
```rust
let mut p: Point = Point { x: 10.0, y: 7.0 };
println!("{:?}", p);
deplace_point(&mut p, 5.0, 8.0);
println!("{:?}", p);
```
```
Point { x: 10.0, y: 7.0 }
Point { x: 15.0, y: 15.0 }
```

## Référer pour ne pas déplacer

Si `Cerlce` était une structure volumineuse, elle ne devrait pas être copiable. Et si l'on souhaitait avoir une fonction `air_cercle` qui retourne l'air du cercle, alors il faudrait prendre le `Cercle` par référence et non par valeur, sinon cette dernière serait déplacée.
```rust
fn air_cercle(c: &Cercle) -> f64 {
	return c.rayon * c.rayon * 3.14159;
}
```

Beaucoup de type ne sont pas copiables. Les prendre par valeur les déplacerait donc dans le cas où le type n'est pas copiable, on est obligé de prendre une référence si on ne veut pas priver l'appelant de sa valeur.

## Référence d'une valeur
Il est possible de prendre l'adresse d'une valeur et pas uniquement d'une variable.
```rust
let a = &0;
```
C'est par exemple ce qui se passe quand on utilise une chaîne de caractère litérale.
```rust
let a: &str = "hello";
```