# Tableaux

## Déclarer un tableau

Déclarer un tableau d'entiers `u32` et de taille `5`. 
```rust
let tableau: [u32; 5] = [7, 2, 3, 0, 8];
```
Il est aussi possible de répéter une valeur :
```rust
let tableau: [u32; 5] = [0; 5];
```
Ce qui équivaut à :
```rust
let tableau: [u32; 5] = [0, 0, 0, 0, 0];
```

## Accès

```rust
let tableau: [u32; 5] = [7, 2, 3, 0, 8];

let a: u32 = tableau[0];
let b: u32 = tableau[3];
```
On accède à une valeur en donnant son indice.

Un dépacement provoquera l'arrêt du programme (*panic*).
```rust
let tableau: [u32; 5] = [7, 2, 3, 0, 8];

let a: u32 = tableau[8]; // PANIC
```

## Index

Essaye d'indexer un tableau avec un entier qui n'est pas de type `usize`:
```rust
let tableau: [u32; 5] = [7, 2, 3, 0, 8];

let index: i16 = 3;
let a: u32 = tableau[index]; // ERROR
```
On ne peut indexer un tableau qu'avec une valeur de type `usize`:
```rust
let tableau: [u32; 5] = [7, 2, 3, 0, 8];

let index: usize = 3;
let a: u32 = tableau[index]; // OK
```

### Explication

Souviens toi, en Rust on ne mélange pas les types, hors, indexer un tableau revient à une addition entre son adresse de début et l'indice.

En C ont aurait:
```cpp
int tableau[5] = {7, 2, 3, 0, 8};

int index = 3;
int a = *(tableau + index);
```

Et en Rust le type `usize` est comme celui d'un pointeur (adresse mémoire), c'est à dire un entier positif de la taille de l'architecture du CPU (32 ou 64 bits).