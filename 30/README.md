# Les génériques

Parfois on souhaite écrire du code sans connaitre à l'avance certains types. Dans ce cas, on peut faire une abstraction du type en question et on dit que notre code est générique.

## Types génériques

Par exemples, si l'on reprend notre structure `Point`.
```rust
struct Point{
	x: f64,
	y: f64,
}
```
On souhaite qu'il soit générique sur le type des coordonnées, que l'on ne soit pas limité à `f64`.
```rust
struct Point<T> {
	x: T,
	y: T,
}
```
Déclarons une variable `p` de type `Point<i32>`.
```rust
let p: Point<i32> = Point<i32>{x: 0, y: 10}; // ERROR
```
Le compilateur se pleins car il croit que l'on souhaite faire une comparaison `Point < i32`. Malheureusement, même si pour un humain, il est clair que ce n'est pas une comparaison mais un générique, syntaxiquement parlant, ce n'est pas différenciable d'une comparaison `a < b`. C'est pourquoi il faut utiliser une notation appelé **turbo fish** (`::<`).
```rust
let p: Point<i32> = Point::<i32>{x: 0, y: 10};
```
Grâce à la déduction du type, on peut racourcir la déclaration.
```rust
let p = Point{x: 0, y: 10};
```

## Fonctions génériques

Implémentons une fonction `carre` qui calcule le carré d'un nombre:
```rust
fn carre(x: f64) -> f64 {
	return x * x;
}
```
Cette fonction prend une valeur de type `f64` et retourne une valeur de type `f64`. Si on veut une fonction `carre` pour le type `i32` alors il faudrait déclarer plusieurs fonctions:
```rust
fn carre_f64(x: f64) -> f64 {
	return x * x;
}
fn carre_i32(x: i32) -> i32 {
	return x * x;
}
```
Il faudrait faire de même pour tous les types avec lesquels on peut être amené à calculer le carré. Ce n'est pas élégant de faire ainsi. Heuresement, il y a une façon d'harmoniser cela, **les génériques**.
On abstrait le type de `x` sous le nom de `T`:
```rust
fn carre<T>(x: T) -> T {
	return x * x; // ERROR
}
```
Seulement voilà, le compilateur ne connaissant pas le type de `x`, il ignore si ce dernier peut être multiplié. Il faut savoir que les types capables de se multiplier implémentent un `trait` nomé `Mul`. Il faut donc restreindre le type `T` à ne pouvoir être qu'un type implémentant `Mul`:
```rust
fn carre<T: std::ops::Mul >(x: T) -> T {
	return x * x; // ERROR
}
```
Le compilateur se pleins, mais cette fois-ci, il peut être difficile de comprendre pourquoi. En effet le trait `Mul` a besoins qu'on lui spécifie le type de retour de la multiplication via un parametre générique appelé `Output`:
```rust
fn carre<T: std::ops::Mul<Output=T> >(x: T) -> T {
	return x * x; // ERROR
}
```
Le compilateur se pleins encore, cette fois ci, c'est à cause de la deuxième utilisation de `x`, le compilateur dit que `x` n'est plus valide car il a été déplacé. Pour comprendre pourquoi `x` est déplacé, le mieux est de voir ce que cache l'opérateur `*`:
```rust
fn carre<T: std::ops::Mul<Output=T> >(x: T) -> T {
	return T::mul(x, x); // ERROR
}
```
En effet, implémenter le trait `Mul` signifie avoir une fonction associé appelée `mul` et c'est cette fonction qui est appelé lorsque l'on utilise l'opérateur `*`. Hors, la variable `x` est déplacée lorsqu'on la place en tant que premier paramètre de `mul`. L'idéal serait que `x` soit un type copiable. On peut rajouter cette contrainte pour résoudre le problème:
```rust
fn carre<T: std::ops::Mul<Output=T> + Copy>(x: T) -> T {
	return T::mul(x, x);
}
```
Enfin, le code compile. Ce peut être décourageant de devoir se confronter à tant de complexité pour faire quelque chose d'aussi simple. Heuresement, on est rarement confronté à ce genre de situation, si j'ai choisi cette situation, c'est parce qu'elle montre bien l'idée derrière les génériques.

## Mot clé `where`

Il existe une notation pour rendre plus lisible les génériques en utilisant le mot clé `where`.
```rust
fn carre<T>(x: T) -> T
where
    T: std::ops::Mul<Output=T>,
    T: Copy,
{
	return T::mul(x, x);
}
```

## Fonction générique ou dynamique

Reprenons notre fonction `perimetre_pga`:
```rust
fn perimetre_pga(a: &dyn Forme, b: &dyn Forme) -> f64 {
	if a.aire() > b.aire() {
		return a.perimetre();
	}
	else {
		return b.perimetre();
	}
}
```
Cette fonction utilise des pointeurs `&dyn`, c'est ce qui lui permet de manipuler des types non déterminés, mais on peut transformer cette fonction pour qu'elle utilise des types génériques.
```rust
fn perimetre_pga<T, U>(a: &T, b: &U) -> f64
where
	T: Forme,
	U: Forme,
{
	if a.aire() > b.aire() {
		return a.perimetre();
	}
	else {
		return b.perimetre();
	}
}
```
Les deux techniques cohabitent parfaitements.
```rust
fn perimetre_pga<T>(a: &T, b: &dyn Forme) -> f64
where
	T: Forme,
{
	if a.aire() > b.aire() {
		return a.perimetre();
	}
	else {
		return b.perimetre();
	}
}
```
Ici `a` est générique, alors que `b` est dynamique.

La différence entre les deux, c'est que le générique est généré au moment de la compilation, c'est à dire que le type `T` de `a` est déterminé au moment où le code compile, alors que le type de `b` reste inconnu même quand le programme s'éxécute.

## Générique mieux que dynamique

Essaye de ne pas utiliser de générique pour coder la fonction `carre` mais à la place des pointeurs `&dyn`. Est-ce que tu as une idée de comment faire ? Malheureusement c'est impossible. Car la fonction `mul` prend ses deux paramêtres par valeur, hors, si on ne connais pas leur taille, c'est impossible.

Avec les génériques, tous les types sont connus au moment de la compilations, ce qui laisse le champs libre au compilateur pour des optimisations plus poussés. En générale, le code générique est très performant.

## Magie noire
La fonction `user_input` fournit dans ce tuto semble pouvoir retourner différents types. Cette fonction est en fait générique, on pourrait la noter `user_input::<i32>()` pour récupérer un `i32`, mais avec la déduction du type, on peut omettre de spécifier le type générique.
Il en va de même pour la fonction `rand::random()` qui peut se noter `rand::random::<i32>()`.
