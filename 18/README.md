# Autres structures

On a vu que l'on pouvait déclarer une struture de la façon suivante:
```rust
struct Point {
	x: f64,
	y: f64,
}

struct Personnage {
	nom: String,
	points_de_vie: u32,
	magie: bool,
}
```

## Façon tuple
Il y a d'autres façons de déclarer une structure comme la façon tuple.
```rust
struct Point(f64, f64);

struct Personnage(String, u32, bool);
```
Ici, les champs ne sont pas només.

On instancie de telle structure comme avec les tuples.
```rust
let p = Point(3.5, 10.0);
```
On vera plus tard comment accéder à leurs champs.

## Vide
Il est même possible qu'une structure soit vide.
```rust
struct Vide;
```
Ce genre de structure est rarement utile, mais ça arrive qu'elle le soit.

On instancie une structure vide comme suit.
```rust
let v = Vide;
```