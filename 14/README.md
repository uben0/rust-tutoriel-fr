# Afficher

On a vu que l'on pouvait afficher des valeurs de la façon suivante:
```rust
let mon_nom: &str = "Eddie";
let mon_age: u32 = 22;
println!("Je m'appelle {}.", mon_nom);
println!("J'ai {} ans.", mon_age);
```

Mais maintenant essayons d'afficher un tableau :
```rust
let mut tableau: [u32; 10] = [0; 10];
println!("{}", tableau); // ERROR
```
Le compilateur se pleins que ce type ne peut pas être affiché. Pour corriger ce problème, il faut remplacer `{}` par `{:?}` :
```rust
let mut tableau: [u32; 10] = [0; 10];
println!("{:?}", tableau); // OK
```
Il existe deux façons d'afficher, l'une est une représentation textuel d'un type (`Display`), l'autre est une inspection de sa composition (`Debug`). Pour bien saisir la différence entre les deux, essayons avec cette chaîne de caractères :
```rust
let s = "Hello \n\tWorld!  ";
println!("display: {}", s);
println!("debug: {:?}", s);
```
Vois-tu en quoi ces deux affichages sont différents ?