# Exemples

## Afficher

```rust
let mon_nom: &str = "Eddie";
let mon_age: u32 = 22;
println!("Je m'appelle {}.", mon_nom);
println!("J'ai {} ans.", mon_age);
```
Ce programme affiche:
```
Je m'appelle Eddie.
J'ai 22 ans.
```
Ce qui permet d'afficher est une **macro** appelée `println`, ce n'est pas une fonction, cela est visible car le nom d'une macro est suivi d'un `!`.
Les macro permettent de faire pleins de choses mais c'est un sujet à voir plus tard, pour le moment comprendre comment utiliser `println` est suffisant.

On peut afficher plusieurs valeurs dans un seul appel à `println`:
```rust
println!("{} {} {} {}", 42, true, "Salut", '💖');
```
```
42 true Salut 💖
```

## Déclaration et appelle d'une fonction

```rust
fn add(a: i32, b: i32) -> i32 {
	return a + b;
}

fn main() {

	let a: i32 = 35;
	let b: i32 = 7;

	let c: i32 = add(a, b);

	println!("{} + {} = {}", a, b, c);
}
```
Ce programme affiche:
```
35 + 7 = 42
```
On remarque que le type de retour d'une fonction est indiqué après ses paramètres par `-> type`. Notons que la fonction `main` ne retourne pas de valeur (il y a la possibilité de lui en faire retourner une, mais ce sera expliqué plus tard).
Comme en C, la fonction `main` ne doit pas être appelée dans le programme, car elle est le point d'entré du programme lui même.