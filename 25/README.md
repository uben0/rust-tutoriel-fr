# La bibliothèque standard

La bibliothèque standard est un ensemble de types, fonctions et traits fournits de base avec le langage.

## Module

Elle est organisée par `mod`ules, un `mod`ule est comme un dossier:
```rust
mod un_dossier {
	fn une_fonction() {
		println!("Salut!");
	}
}
```
On accède à ses éléments grâce à son nom suivit du séparateur `::`
```rust
un_dossier::une_fontcion();
```
On peut imbriquer des `mod`ules, y déclarer des fonctions avec le même nom sans les mélanger:
```rust
mod a {
	fn une_fonction() {
		println!("Bonjour");
	}

	mod b {
		fn une_fonction() {
			println!("Aurevoir");
		}
	}
}
mod b {
	fn une_fonction() {
		println!("Bonsoir");
	}
}
```
```rust
a::une_fonction();
a::b::une_fonction();
b::une_fonction();
```
```
Bonjour
Aurevoir
Bonsoir
```

## Le module `std::`

Le `mod`ule `std::` contient la bibliothèque standard. On peut accéder à sa documentation en éxécutant la commande:
```bash
rustup doc --std
```

Cependant, beaucoup des types ou traits de la bibliothèque standard sont rendus directement accessibles sans avoir à spécifier leur chemin, par exemple, on peut utiliser `Option` sans avoir à indiquer `std::option::Option`.

## Mot clé `use`

Il est possible de rendre directement accessible un élément voulu grâce au mot clé `use`:
```rust
use a::b::une_fonction;

une_fontion();
```

## C'est standard, c'est bien

La bibliothèque standard défini une multitudes de types, de fonctions et de traits très utiles. Le mieux est de découvrir au fur et à mesure différents éléments de cette dernière. Ce chapitre est là surtout pour bien faire comprendre que la bibliothèque standard est omniprésente, à titre d'exemple, on a utilisé la fonction `f64::sqrt()` qui est définit dans la bibliothèque standard. Il y a aussi `print` et `println`.

## Générique

Tu veras beaucoup de types, fonctions et traits génériques. Un type est générique quand il utilise un ou plusieurs types qu'il ne connait pas encore. Les génériques sont notés avec `<T, U, ...>` comme `Option<T>`. Je reviendrais sur les génériques plus tard.
