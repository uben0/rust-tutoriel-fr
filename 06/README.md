# Fonctions

Déclarons une fonction qui additionne deux valeurs de type `u32`:
```rust
fn additionne(a: u32, b: u32) -> u32 {
    return a + b;
}
```

Déclarons une fonction qui affiche `Hello World!`:

```rust
fn hello_world() -> () {
    println!("Hello World!");
}
```

Cette fonction ne prend aucun paramètre et ne retourne pas de valeur. Cependant, une fonction doit toujours retourner une valeur, donc on lui fait retourner `()` qui est le tuple vide. Par défaut, si on ne précise pas le type de retour, ce dernier sera implicitement `()`:

```rust
fn hello_world() {
    println!("Hello World!");
}
```

## Nomenclature
Les fonctions doivent avoir un nom en *camel_case* sinon un avertissement est émit:
```rust
fn helloWorld() { // WARNING
    println!("Hello World!");
}
```

## Appel

```rust
hello_world();

let m = 42;
let n = additionne(67, m);
```