# Match

Il existe un embranchement spécialisé pour matcher des paternes. Il fonctionne avec n'importe quel paterne.

```rust
match event {
    Event::Quit => {
        println!("la fête est finie");
        return
    }
    Event::Click (x, y) => {
        if x == y {
            println!("diagonal!");
        }
    }
    Event::Keyboard { key: _, shift: false } => {
        println!("qui tapote, vivote");
    }
    Event::Keyboard { key, shift: true } => {
        println!("PAS LA PEINE DE CRIER {}", key);
    }
}
```
Il permet une écriture plus compacte et concise que le `if let` :

```rust
fn message_jour(j: JourSemaine) {
    match j {
        JourSemaine::Lundi => println!("Je n'irai pas travailler"),
        JourSemaine::Mardi => println!("Je prépare des crèpes"),
        JourSemaine::Mercredi => println!("Je fais la sieste"),
        JourSemaine::Jeudi => println!("C'est manif !"),
        JourSemaine::Vendredi => println!("Le weekend, que c'est bon."),
        JourSemaine::Samedi => println!("Gilets jaunes"),
        JourSemaine::Dimanche => println!("Je reste sous la couette"),
    }
}
```


## Fibonacci
Créons une fonction qui retourne le n'ième terme de la suite de Fibonacci.
```rust
fn fibonacci(n: usize) -> u128 {
    match n {
        0 => 0,
        1 => 1,
        x => fibonacci(n - 1) + fibonacci(n - 2)
    }
}
```
Élégant non ?

Si `n` vaut `0` ou `1`, il peut matcher avec le paterne `x`, mais les paternes sont testés dans l'ordre, donc on peut être sure que les deux paternes `0` et `1` captureront respectivement les valeurs `0` et `1`.