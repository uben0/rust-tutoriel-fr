# Jeu du nombre mystère

Tu vas coder ton premier jeu en Rust, il s'agira du **Nombre mystère**. Un nombre mystère est choisi entre `0` et `99` inclu et tu dois le deviner. Tu as `10` essais, à chaque fois, il est indiqué si le nombre que tu as entré est plus petit ou plus grand que le nombre mystère.

Exemple :
```
Le nombre mystère a été choisi. Devine-le !

Ta réponse :
40
Le nombre mystère est plus petit que 40.

Ta réponse :
10
Le nombre mystère est plus grand que 10.

Ta réponse :
25
Le nombre mystère est plus grand que 25.

Ta réponse :
32
Bravo, tu as deviné le nombre mystère !
```

Tu as tous les outils en mains pour réaliser ce jeu, sauf un, celui qui te permet de choisir un nombre aléatoir.

## Générer un nombre aléatoir

Le plus simple est de générer un entier aléatoir.
```rust
let random_integer: u32 = rand::random();
```
Et de le ramener à un interval avec un modulo.
```rust
let mystere: u32 = random_integer % 100;
```
La variable `random_integer` peut avoir n'importe quelle valeur d'un entier `u32` (de `0` à `4294967295`), grâce au modulo `% 100`, on s'assure que la variable `mystere` soit comprise entre `0` et `99` inclu.

# Correction
Une correction se trouve dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/10/correction.rs">`correction.rs`</a>.
