# Jeu d'échecs (3)

## Saisir le mouvement de pièce

On souhaite que l'utilisateur entre les coordonnés de la pièce qu'il souhaite déplacer ainsi que les coordonnés où il souhaite la déplacer.
Par exemple `"e5f6"` ou encore `"a1a2"`.

Créons une structure représentant un mouvement:
```rust
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Move {
	src_x: usize,
	src_y: usize,
	dst_x: usize,
	dst_y: usize,
}
```
Créons une fonction qui interprète ce que rentre l'utilisateur pour renvoyer un `Move`. Je fournie la fonction `user_input`:
```rust
fn user_input<T: std::str::FromStr>() -> T
where <T as std::str::FromStr>::Err: std::fmt::Debug
{
	let mut l = String::new();
	std::io::stdin().read_line(&mut l).unwrap();
	T::from_str(l.as_str().trim()).unwrap()
}
```

Commençons par le prototype.

```rust
fn get_user_move() -> Option<Move> {

}
```
Dans un premier temps, on récupère ce que l'utilisateur a tapé dans une `String`:

```rust
fn get_user_move() -> Option<Move> {
	println!("enter move:");
	let s: String = user_input();
}
```

Maintenant on veut faire correspondre les lettres de `'a'` jusqu'à `'h'` à des indice de `0` à `7` inclu.
```rust
fn get_user_move() -> Option<Move> {
	println!("enter move:");
	let s: String = user_input();
	let src_x = match s[0] { // ERROR
		'a' => 0,
		'b' => 1,
		'c' => 2,
		'd' => 3,
		'e' => 4,
		'f' => 5,
		'g' => 6,
		'h' => 7,
		_ => return None
	};
}
```

On ne peut pas faire `s[0]` car `s` est une `String` qui représente une chaîne de caractère unicode encodé en UTF8. Hors un caractère unicode en UTF8 peut faire un ou plusieurs octets de large, et par convention, l'opérateur d'indexion `[n]` doit avoir une complixité constante, hors avec des caractères ayant des tailles variables, il est impossible de connaitre à l'avance l'emplacement du n-ième caractère. Mais il y a une solution.

On peut utiliser un itérateur (la méthode `chars` de `String` retourne un itérateur sur les caractères). La méthode `nth` d'un itérateur retourne le n-ième élément de l'itérateur.
```rust
let s = String::from("こんにちは世界");

println!("{:?}", s.chars().nth(3));
```
```
Some('ち')
```

Corrigeons notre code.
```rust
fn get_user_move() -> Option<Move> {
	println!("enter move:");
	let s: String = user_input();
	let mut iter = s.chars();

	let src_x = match iter.next()? {
		'a' => 0,
		'b' => 1,
		'c' => 2,
		'd' => 3,
		'e' => 4,
		'f' => 5,
		'g' => 6,
		'h' => 7,
		_ => return None
	};
}
```
En utilisant l'opérateur `?`, on gère le cas où `next` retournerai `None`.

On peut compresser le code.
```rust
fn get_user_move() -> Option<Move> {
	println!("enter move:");
	let s: String = user_input();
	let mut iter = s.chars();

	let src_x = match iter.next()? {
		'a' => 0, 'b' => 1, 'c' => 2, 'd' => 3,
		'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7,
		_ => return None
	};
}
```
On fait cette conversion avec les trois autres caractères:
```rust
fn get_user_move() -> Option<Move> {
	println!("enter move:");
	let s: String = user_input();
	let mut iter = s.chars();

	let src_x = match iter.next()? {
		'a' => 0, 'b' => 1, 'c' => 2, 'd' => 3,
		'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7,
		_ => return None
	};
	let src_y = match iter.next()? {
		'1' => 0, '2' => 1, '3' => 2, '4' => 3,
		'5' => 4, '6' => 5, '7' => 6, '8' => 7,
		_ => return None
	};
	let dst_x = match iter.next()? {
		'a' => 0, 'b' => 1, 'c' => 2, 'd' => 3,
		'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7,
		_ => return None
	};
	let dst_y = match iter.next()? {
		'1' => 0, '2' => 1, '3' => 2, '4' => 3,
		'5' => 4, '6' => 5, '7' => 6, '8' => 7,
		_ => return None
	};
}
```

On retourne un `Move` ensuite:
```rust
fn get_user_move() -> Option<Move> {
	println!("enter move:");
	let s: String = user_input();
	let mut iter = s.chars();

	let src_x = match iter.next()? {
		'a' => 0, 'b' => 1, 'c' => 2, 'd' => 3,
		'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7,
		_ => return None
	};
	let src_y = match iter.next()? {
		'1' => 0, '2' => 1, '3' => 2, '4' => 3,
		'5' => 4, '6' => 5, '7' => 6, '8' => 7,
		_ => return None
	};
	let dst_x = match iter.next()? {
		'a' => 0, 'b' => 1, 'c' => 2, 'd' => 3,
		'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7,
		_ => return None
	};
	let dst_y = match iter.next()? {
		'1' => 0, '2' => 1, '3' => 2, '4' => 3,
		'5' => 4, '6' => 5, '7' => 6, '8' => 7,
		_ => return None
	};
	Some(Move {
		src_x: src_x,
		src_y: src_y,
		dst_x: dst_x,
		dst_y: dst_y,
	})
}
```


On peut encore racourcir le code:
```rust
fn get_user_move() -> Option<Move> {
	println!("enter move:");
	let s: String = user_input();
	let mut iter = s.chars();
	Some(Move {
		src_x: match iter.next()? {
			'a' => 0, 'b' => 1, 'c' => 2, 'd' => 3,
			'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7,
			_ => return None
		},
		src_y: match iter.next()? {
			'1' => 0, '2' => 1, '3' => 2, '4' => 3,
			'5' => 4, '6' => 5, '7' => 6, '8' => 7,
			_ => return None
		},
		dst_x: match iter.next()? {
			'a' => 0, 'b' => 1, 'c' => 2, 'd' => 3,
			'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7,
			_ => return None
		},
		dst_y: match iter.next()? {
			'1' => 0, '2' => 1, '3' => 2, '4' => 3,
			'5' => 4, '6' => 5, '7' => 6, '8' => 7,
			_ => return None
		},
	})
}
```

On souhaiterait une fonction qui boucle tant que l'utilisateur n'a pas rentré un mouvement syntaxiquement correct.

```rust
fn get_user_move_loop() -> Move {
	loop {
		if let Some(m) = get_user_move() {
			return m
		}
		println!("invalid syntax");
	}
}
```

## Progression du projet
Trouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/39/main.rs">`main.rs`</a> le projet.
