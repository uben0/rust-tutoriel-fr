# Déduction du type

Il n'est pas nécessaire de préciser un type si ce denier peut être déduit du context.
```rust
let a: i32 = 4;
let b = a;
```
Ici, `b` est de type `i32` car `a` est de type `i32`. Par ailleur, si on indiquait que `b` était de type `u32`, cela provoquerai une erreur, rappels-toi, en Rust, on ne mélange pas les types, contrairement au C où des conversions se font implicitement.


## Type sur literal

On peut indiquer le type sur un literal :
```rust
let a: i32 = 4i32;
let b: u32 = 6u32;
```
Donc, on peu racourcir:
```rust
let a = 4i32;
let b = 6u32;
```

## Type par défaut

Si aucun type ne peut être induit sur un literal, alors il existe un type par défaut, `i32` pour les entiers et `f64` pour les décimaux.
```rust
let a = 4;    // i32
let b = 6.0;  // f64
```

## Autres types

```rust
let c = 'あ';       // char
let b = true;       // bool
let t = [0, 7, 1];  // [i32; 3]
let s = "hello";    // &str
let v = (true, 9);  // (bool, i32)
```