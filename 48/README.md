# Jeu d'échecs (8)

Améliorons le code de notre projet.

## Self

Appliquons ce que nous avons appris avec les mots clé `Self` et `self` (voir chapitre précédent).

## Itérateur de pièce

On se retrouve souvent à itérer sur les pièces du plateau. On va créer un itérateur sur les pièces du plateau.
```rust
struct BoardIter<'a> {
    board: &'a [[Option<Piece>; 8]; 8],
    x: usize,
    y: usize,
}
```
```rust
impl<'a> Iterator for BoardIter<'a> {
    type Item = (usize, usize, Piece);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            // si on est à la fin
            if self.y == 8 {
                return None
            }
            // on garde de coté les coordonnés actuelle
            let (x, y) = (self.x, self.y);
            // on avance d'un pas
            self.x += 1;
            // si on est en fin de ligne
            if self.x == 8 {
                // sauter une ligne
                self.y += 1;
                // revenir à la première colone
                self.x = 0;
            }
            // si il y a une pièce
            if let Some(p) = self.board[y][x] {
                return Some((x, y, p))
            }
        }
    }
}
```
Créons une méthode `pieces` de `Game` qui renvoie cette itérateur.
```rust
fn pieces(&self) -> BoardIter {
    BoardIter{
        board: &self.board,
        x: 0, y: 0,
    }
}
```

On peut simplifier toutes les fonctions qui itère sur les pièces du plateau. Comme `is_in_check`.
```rust
fn is_in_check(&self, player: Color) -> bool {
    let mut map = [[false; 8]; 8];
    let oponent = match player {Black=>White, White=>Black};
    self.reachable_by_player(oponent, &mut map);
    for y in 0..8 {
        for x in 0..8 {
            if let Some(piece) = self.board[y][x] {
                if map[y][x] && piece.c == player && piece.t == King {
                    return true;
                }
            }
        }
    }
    false
}
```
Qui devient:
```rust
fn is_in_check(&self, player: Color) -> bool {
    let mut map = [[false; 8]; 8];
    let oponent = match player {Black=>White, White=>Black};
    self.reachable_by_player(oponent, &mut map);
    for (x, y, piece) in self.pieces() {
        if map[y][x] && piece.c == player && piece.t == King {
            return true;
        }
    }
    false
}
```
On peut même faire mieux en utilisant la méthode `any` d'un itérateur.
```rust
fn is_in_check(&self, player: Color) -> bool {
    let mut map = [[false; 8]; 8];
    let oponent = match player {Black=>White, White=>Black};
    self.reachable_by_player(oponent, &mut map);
    self.pieces().any(|(x, y, p)| map[y][x] && p.c == player && p.t == King)
}
```

## Coordonnés
Modifions la méthode `print` de `Game` de sorte qu'elle affiche les coordonnés des cases.
```rust
fn print(&self) {
    println!("  a b c d e f g h");
    for y in 0..8 {
        print!("{} ", y + 1);
        for x in 0..8 {
            match self.board[y][x] {
                Some(piece) => {
                    print!("{} ", piece.to_unicode());
                }
                None => {
                    print!("  ");
                }
            }
        }
        println!(" {}", y + 1);
    }
    println!("  a b c d e f g h");
}
```

## Progression

Retrouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/48/main.rs">`main.rs`</a> le projet.
