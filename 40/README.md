# À qui appartient quoi ?

En Rust, il n'y a jamais d'ambiguïté quant à l'apartenance d'une valeur.

Prennons la fonction C `ctime` de `"time.h"`.
```cpp
char *ctime(const time_t *timep);
```
Cette fonction retourne un pointeur sur une chaîne de caractère. Sans la documentation, il est impossible de savoir si l'on possède la chaîne retournée, ou si elle nous est juste prétée.

Si l'on possède cette chaîne, on est responsable de son allocation, donc de la libérer (appeler `free()`) quand on en a plus besoins. Si cette chaîne est prétée, on ne la possède pas, on ne doit pas la désallouer.

Essaye de trouver la documentation et de savoir si `ctime` passe ou prête la chaîne de caractère. Est-ce facile à trouver ?

## Règles d'appartenance

En Rust, en regardant le type d'une valeur, on peut déterminer dans quelle mesure elle nous appartient.

| type   | lecture | écriture | prenable | on dit que |
|--------|:-------:|:--------:|:--------:|----------------|
|`T`     |       ✓ |        ✓ |        ✓ | on possède `T`
|`&T`    |       ✓ |          |          | on emprunte `T` en lecture seule
|`&mut T`|       ✓ |        ✓ |          | on emprunte `T` en lecture et écriture


## Exemple
```rust
struct Data {
    a: i32;
    b: u32;
    c: char;
}


fn take(v: Data) {}

fn borrow(v: &Data) {}

fn borrow_mut(v: &mut Data) {}
```
Ces trois fonctions ne sont pas autorisées à disposer de la valeur de type `Data` de la même façon.

- `take` possède la valeur, elle a tous les droits, elle peut même la déplacer ou la détruire.
- `borrow` emprunte la valeur en lecture seul, elle peut la lire et la préter en lecture seule à son tour autant qu'elle veut.
- `borrow_mut` emprunte la valeur en lecture et écriture, elle peut la lire et la modifier, la préter en lecture seule ou avec écriture autant qu'elle veut.

## Emprunter ou posseder

Pour comprendre les règles fixées par le langage Rust on peut utiliser une analogie avec le pret d'un livre.

Si tu possèdes un livre, alors tu es libre de le lire, d'écrire dessus, de le préter ou de le donner à quelqu'un, ou encore de le dértuire.

Si on t'a prété un livre, tu devra le rendre tel qu'on te l'a passé, donc tu peux le lire et le préter autant que tu veux sous la seule condition qu'on te le retourne intact pour que tu puisse le retourner intact.

Si on t'as prété un livre mais qu'on t'as autorisé à écrire dedans alors tu peut tout à fait si tu le désire le préter autant que tu veux en autorisant qu'on écrive dedans, mais il faudra qu'on te le rende pour pouvoir le rendre.

Si tu donnes un livre, alors il ne t'appartient plus, tu ne peux plus le lire ou écrire dedans, tu ne peux bien sûre plus le préter. Il n'est plus à toi.

Si tu prète un livre à quelqu'un, alors tu n'y a plus accès tant qu'il ne te le rend pas.

Cependant, on peut partager un livre, c'est à dire le préter mais n'autoriser que la lecture, comme ça on peut être plusieur à le lire en même temps.

Pour les valeurs en Rust, c'est pareil. Ces règles garantissent l'intégrité des données. Elles peuvent être contraignantes mais ce qu'elles garantissent est très précieux. C'est le compilateur qui s'assure qu'elles sont bien respectées.

## Duré de vie

Si un livre a une duré de vie, alors il ne peut pas être emprunté plus longtemps que cette duré de vie.

C'est pareil avec les valeur en Rust.
```rust
fn ref_of_dead_value() -> &i32 {
    let d = 42;
    return &d; // ERROR
}
```
Ici, on essai de préter `d` à l'appelant alors que `d` meurt lorsque la fonction se finit (c'est une variable locale). Et le compilateur va le voir. Ici, c'est évident qu'il y a un problème, mais peu importe l'agencement du code, ou sa complixité, le compilateur est garantie de détecter si une durée de vie n'est pas respectée.

## Duré de vie explicite

```rust
struct RefData {
    ref_data: &i32, // ERROR
}
```
Si on imagine une structure qui stocke une référence vers une valeur de type `i32`, alors le compilateur se pleins qu'on ne spécifie pas la durée de vie de la valeur référés par `ref_data`. Pour corriger le problème, il faut que notre structure `RefData` soit générique sur une duré de vie. Ça parait un peu complexe et étrange mais en pratiquant un peu on se rend vite compte que ce n'est pas sorcier.
```rust
struct RefData<'a> {
    ref_data: &'a i32,
}
```
```rust
let d = 42;

let ref_d = RefData{ref_data: &d};
```

## Ambiguïté

À chaque fois qu'il y a une référence, il y a une duré de vie qui est calculée. Pourtant, on a utilisé des références avant et on a jamais eu à préciser la duré de vie. Tant qu'il n'y a pas d'ambiguïté sur la duré de vie de la valeur référée, il n'y a pas besoins de la spécifier.
```rust
fn print_msg(msg: &String) {
    println!("{}", msg);
}
```
Ici, on sait pour sûre que la référence vie moins longtemps que la valeur référée. la référence ne vie que durant l'exécution de la fonction, alors que la valeur référé n'a pas le droit d'être détruite tant qu'elle est prétée.

### Ambigu

Imaginons une fonction qui concatenne deux chaînes de caractères, et qui retourne au passage une référence sur celle contenant le résultat.
```rust
fn append(s1: &mut String, s2: &String) -> &String // ERROR
{
	s1.push_str(s2);
	return s1;
}
```
En ne se fiant qu'au prototype, on ne sait pas quelle est la duré de vie de la valeur référée par la référence retournée. En regardant le corps de la fonction, on peut voir que c'est celle de `s1`. Mais il faut le spécifier explicitement.
```rust
fn append<'a>(s1: &'a mut String, s2: &String) -> &'a String {
	s1.push_str(s2);
	return s1;
}
```
Maintenant, juste en regardant le prototype, il n'y a plus d'ambiguïté.

### Non ambigu
```rust
fn append_hello(msg: &mut String) -> &mut String {
	msg.push_str("hello");
	return msg;
}
```
Ce cas est est considéré comme non ambigu car on considère que quand une seule référence est passée en paramètre, alors celle renvoyée a la même duré de vie.

## Dispute avec le compilateur

Les règles d'appartenance et de duré de vie sont les plus difficiles à respecter. Il y a tout à parier que tu vas t'arracher les cheveux en n'arrivant pas à satisfaire le compilateur sur une de ces règles. Il est important de prendre le temps de lire ce que répond le compilateur, car il ne se contente pas de reffuser, mais il donne aussi des expliquations détaillées de pourquoi il reffuse et t'aiguille sur les sollutions possibles.

### Problème incontournable

Souvent, quand le compilateur se pleins que tu enfreins une règles d'appartenance ou de duré de vie, il est impossible de contourner le problème, ce n'est pas ce que tu fais qui n'est pas correcte, mais la façon de faire. Ça demande de revoir le modèle de son code. Souvent, ces règles nous force à adopter un style de programmation proche du fonctionnel.

### Problème contournable

Heursement, il est possible, dans une certaine mesure, de s'affranchir des règles d'appartenance et de duré de vie, pas directement, mais indirectement en utilisant des outils de la bibliothèque standard (ou de bibliothèques tiers). Ce sera le sujet d'un prochain chapitre.
