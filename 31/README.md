# Option

Une énumération appelée `Option` est définit dans la bibliothèque standard.
```rust
enum Option<T> {
    Some(T),
    None,
}
```

Cette énumération représente l'idée qu'une valeur n'est pas forcément présente. Par exemple, imaginons une fonction qui cherche un élément dans un tableau et retourne son indice.

```rust
fn cherche_tableau_u32(t: &[u32], n: u32) -> usize {
    for i in 0 .. t.len() {
        if t[i] == n {
            return i;
        }
    }
    // ERROR
}
```
Cette dernière pourrait échouer si par exemple la valeur cherchée n'est pas dans le tableau. Il faut un moyen de coder l'idée qu'il peut ou non y avoir un résultat.
```rust
fn cherche_tableau_u32(t: &[u32], n: u32) -> Option<usize> {
    for i in 0 .. t.len() {
        if t[i] == n {
            return Option::Some(i);
        }
    }
    Option::None
}
```

## Générique

Le type `Option<T>` est générique, ça signifie que l'on peut choisir le type stocké dans la variante `Option::Some(T)`, ici on a choisi `Option<usize>` donc on a la variante `Option::Some(usize)`.

Ici c'est un cas simple de générique, mais ces derniers se couple avec d'autres mécanisme tel que les traits et ça devient vite très complexe. On va se familiariser avec petit à petit.

## Accessibilité

Il n'y a pas besoins de noter `Option::Some` ou `Option::None`, on peut directement noter `Some` et `None`.
```rust
fn cherche_tableau_u32(t: &[u32], n: u32) -> Option<usize> {
    for i in 0 .. t.len() {
        if t[i] == n {
            return Some(i);
        }
    }
    None
}
```

## Méthodes

Le type `Option` a une multitude de méthode très utiles.

- La méthode `unwrap` qui, si l'`Option` est de la variante `Some` retourne la valeur contenu dans la variante `Some`, ou qui stop le programme si la variante est `None`. C'est pratique quand on sait que l'`Option` doit être de la variante `Some`.
- La méthode `is_some` qui retourne `true` si l'`Option` est de la variante `Some`, et `false` sinon.
- La méthode `is_none` qui retourne `true` si l'`Option` est de la variante `None`, et `false` sinon.
- La méthode `map` qui modifie la valeur contenue s'il y en a une.

Je t'invite à explorer la documentation pour voir toutes les possiblités.

## Opérateur `?`

Il y a un opérateur très pratique sur les `Option`s.

Imaginons une fonction qui retourne la plus petite valeur d'un tableau.
```rust
fn minimum_tableau_u32(t: &[u32]) -> Option<u32> {
    if t.len() == 0 {
        None
    }
    else {
        let mut min = 0;
        for i in 1 .. t.len() {
            if t[i] < t[min] {
                min = i;
            }
        }
        Some(t[min])
    }
}
```
On est obligé de retourner une `Option` car si le tableau est vide, il n'y a pas de minimum.

Et imaginons une fonction aui retourne l'indice de la plus petite valeur d'un tableau, en utilisant `cherche_tableau_u32` et `minimum_tableau_u32`.

```rust
fn minimum_tableau_u32_i(t: &[u32]) -> Option<usize> {
    let min = match minimum_tableau_u32(t) {
        Some(min) => min,
        None => return None,
    }
    cherche_tableau_u32(t, min)
}
```

On peut utiliser l'opérateur `?` pour ne pas avoir à écrire le `match`.
```rust
fn minimum_tableau_u32_i(t: &[u32]) -> Option<usize> {
    let min = minimum_tableau_u32(t)?;
    cherche_tableau_u32(t, min)
}
```
Pratique non ? Cet opérateur fera retourner `None` à la fonction si la valeur `Option` sur laquelle il est appelé est `None`.

On peut encore racourcir.
```rust
fn minimum_tableau_u32_i(t: &[u32]) -> Option<usize> {
    cherche_tableau_u32(t, minimum_tableau_u32(t)?)
}
```
