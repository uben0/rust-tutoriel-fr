# Tout a une valeur

En Rust, les blocks, les embranchements et les fonctions valent toujours quelque chose. C'est à dire qu'ils retournent toujours une valeur.

## block
Il est possible d'écrire:
```rust
let a = 3;

let b = {
    let c = a + 4;
    let d = c / 2;
    d * 3
};
```
La dernière instruction du block ne se termine pas par un point virgule, ça signifie qu'elle sera retournée du block et affectée à `b`.

## if else
Par exemple, on peut remplacer ce code:
```rust
let mut i;
if condition {
    i = 0;
}
else {
    i = 1;
}
```
Par:
```rust
let i = if condition {0} else {1};
```
C'est mieu non ?

## fonction
On peut remplacer:
```rust
fn add(a: u32, b: u32) -> u32 {
    return a + b;
}
```
Par:
```rust
fn add(a: u32, b: u32) -> u32 {
    a + b
}
```

## Par défaut

Si aucune valeur de retour n'est placé à la fin d'un block, alors sa valeur de retour sera `()`, qui est en Rust le type vide. Savoir cela aide à comprendre les messages d'erreur du compilateur, qui souvent se plein qu'on lui passe un `()` là où un autre type est attendu.

## Façon fonctionnelle
Cette façon qu'ont les blocks et les embranchement d'avoir une valeur est inspirée de la programmation fonctionnelle (comme les langages Ocaml et Haskell). Ce qui permet de coder d'une façon élégante.
```rust
fn maximum_u32(a: u32, b: u32) {
    if a > b {a} else {b}
}
```