# Jeu d'échecs (1)

Créons un jeu d'échecs qui se joue dans le terminal.

## Les pièces

Créons une énumération représentant le type d'une pièce. On utilise l'anglais, c'est une bonne habitude à prendre. Utilisons la directive `#[derive()]` pour que certains traits biens utiles soit implémentés automatiquement:
```rust
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum PieceType {
    King,
    Queen,
    Rook,
    Knight,
    Bishop,
    Pawn,
}
```
Nous connaissons déjà `Debug`, `Clone` et `Copy`, mais pas `PartialEq` et `Eq`, ils rendent possible la comparaison avec `==` pour tester si deux valeurs de type `PieceType` sont égales.

Créons une énumération pour la couleur:

```rust
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Color {
    Black,
    White,
}
```

Puis enfin, une structure représentant une pièce:
```rust
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Piece {
    t: PieceType,
    c: Color,
}
```

## Accès direct avec `use`

Pour créer une pièce on doit écrire:
```rust
let p = Piece { t: PieceType::Pawn, c: Color::Black };
```
C'est un peut long, on aimerai ne pas avoir à spécifier `PieceType` et `Color` devant `Pawn` et `Black`. C'est possible grâce au mot clé `use`:
```rust
use Color::Black;
use Color::White;

use PieceType::King;
use PieceType::Queen;
use PieceType::Rook;
use PieceType::Knight;
use PieceType::Bishop;
use PieceType::Pawn;
```
On peut améliorer cette notation avec `{ }`:
```rust
use Color::{Black, White};
use PieceType::{King, Queen, Rook, Knight, Bishop, Pawn};
```
On peut aller plus loins quand on prend tous les éléments avec le symbol `*`:
```rust
use Color::*;
use PieceType::*;
```
Et voilà, on peut maintenant écrire:
```rust
let p = Piece { t: Pawn, c: Black };
```

## Progression du projet
Trouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/36/main.rs">`main.rs`</a> le projet.
