# Jeu d'échecs (2)

## Le plateau

Créons le plateau de jeu qui sera un tableau en deux dimension de `8` par `8`. Nous le stockerons dans une structure `Game` représentant le jeu:
```rust
#[derive(Debug, Clone, PartialEq, Eq)]
struct Game {
    board: [[Piece; 8]; 8],
}
```
Notez que l'on ne dérive pas `Copy`, car `Game` est une structure volumineuse. On ne souhaite pas qu'elle soit copiable, en revanche elle est tout à fait clonable.

Sur un emplacement, il peut y avoir une pièce comme il peut ne pas y en avoir une. Mais le problème est que le type de `board` est un tableau de pièce, il faut changer ça pour pouvoir faire la différence entre un emplacement avec une pièce et un emplacement sans pièce. On peut déclarer une énumération qui fait cela:
```rust
enum Place {
    Occupied(Piece),
    Free,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Game {
    board: [[Place; 8]; 8],
}
```

Cependant, il existe déjà l'énumération `Option` déclarée dans la bibliothèque standard.
```rust
enum Option<T> {
    Some(T),
    None,
}
```
C'est une bonne habitude à avoir que d'utiliser les outils de la bibliothèque standard au lieu de les recoder soi-même.
```rust
#[derive(Debug, Clone, PartialEq, Eq)]
struct Game {
    board: [[Option<Piece>; 8]; 8],
}
```

## Constructeur

On va ajouter un constructeur à `Game` qui initialise le plateau avec les pièces qu'il faut:
```rust
impl Game {
    fn new() -> Game {
        let mut g = Game {
            board: [[None; 8]; 8],
        };

        let line = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook];

        for x in 0..8 {
            g.board[0][x] = Some(Piece{t: line[x], c: Black});
            g.board[1][x] = Some(Piece{t: Pawn,    c: Black});
            g.board[6][x] = Some(Piece{t: Pawn,    c: White});
            g.board[7][x] = Some(Piece{t: line[x], c: White});
        }

        return g;
    }
}
```

## Affichage

Pour afficher le jeu dans le terminal, on peut utiliser les caractères unicode de pièces d'échecs et avoir une méthode sur `Piece` qui renvoi le caractère correspondant:
```rust
impl Piece {
    fn to_unicode(self: Piece) -> char {
        match (self.c, self.t) {
            (Black, King  ) => '♚',
            (Black, Queen ) => '♛',
            (Black, Rook  ) => '♜',
            (Black, Knight) => '♞',
            (Black, Bishop) => '♝',
            (Black, Pawn  ) => '♟',
            (White, King  ) => '♔',
            (White, Queen ) => '♕',
            (White, Rook  ) => '♖',
            (White, Knight) => '♘',
            (White, Bishop) => '♗',
            (White, Pawn  ) => '♙',
        }
    }
}
```

Et ajouter une méthode `print` dans le block `impl` de `Game`:
```rust
fn print(self: &Game) {
    for y in 0..8 {
        for x in 0..8 {
            match self.board[y][x] {
                Some(piece) => {
                    print!("{} ", piece.to_unicode());
                }
                None => {
                    print!("  ");
                }
            }
        }
        println!("");
    }
}
```

On peut créer un `Game` et l'afficher:
```rust
fn main() {
    let g = Game::new();
    g.print();
}
```
Ce qui donne:
```
♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜ 
♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟ 
                
                
                
                
♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙ 
♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖ 
```

## Progression du projet
Trouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/37/main.rs">`main.rs`</a> le projet.
