# Utiliser un itérateur

Le trait `Iterator` vient avec pleins de méthodes et autres outils.

## Ignorer

On peut passer les n-premiers éléments d'un itérateur.
```rust
let fib = Fibonacci::new();
let mut fib = fib.skip(10);

println!("fibonacci(11) = {:?}", fib.next());
```

## De dix en dix

On peut soter par pas de n-éléments.
```rust
let fib = Fibonacci::new();
let mut fib = fib.step_by(10);

println!("fibonacci(0) = {:?}", fib.next());
println!("fibonacci(10) = {:?}", fib.next());
println!("fibonacci(20) = {:?}", fib.next());
```

## Borner

On peut limiter le nombre d'éléments.
```rust
let fib = Fibonacci::new();
let mut fib = fib.take(3);

println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
```
```
Some(0)
Some(1)
Some(1)
None
```

## Maper

On peut modifer les éléments un à un. Ici, multiplier par `10`.
```rust
let fib = Fibonacci::new();
let mut fib = fib.map(|n| n * 10);

println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
```
```
Some(0)
Some(10)
Some(10)
Some(20)
```
On vera plus tard la notation `|n| n * 10`.

## Combinaison
On peut combiner tous ce qu'on a vu.
```rust
let fib = Fibonacci::new();
let fib = fib.map(|n| n * 10);
let fib = fib.step_by(2);
let fib = fib.skip(8);
let mut fib = fib.take(3);

println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
```
```
Some(9870)
Some(25840)
Some(67650)
None
```
Toute les méthodes que l'on utilise absorde l'itérateur dans un nouvel itérateur. Seul le dernier, celui sur le quel on appelle `next`, a besoins d'être mutable.

On peut réécrire le code précédent sous une forme plus compacte.
```rust
let mut fib = Fibonacci::new()
    .map(|n| n * 10)
    .step_by(2)
    .skip(8)
    .take(3)
;

println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
```

## Des couches

Chaque appel à ces méthode rajoute une couche, on peut voir ça comme les couches d'un ognon. À la fin le type de `fib` est :
```rust
Take< Skip< StepBy< Map< Fibonacci > > > >
```
Les types `Take`, `Skip`, `StepBy` et `Map` sont génériques. On a donc un générique de générique de générique de générique, et ça fonctionne à la perfection.

## Boucle for

On peut utiliser la boucle for pour itérer sur un itérateur.
```rust
for n in Fibonacci::new() {
    println!("{}", n);
}
```
Mais c'est une boucle est infinie, car la boucle `for` ne s'arrête que lorsque l'itérateur a retourné un `None`. On peut utiliser `take` pour limiter le nombre d'éléments.
```rust
for n in Fibonacci::new().take(10) {
    println!("{}", n);
}
```

## La documentation

Tout est dans la documentation, il faut lire la documentation. Pour rappel, on ouvre la documentation en éxécutant :
```bash
rustup doc --std
```
Souviens toi de cette abréviation, c'est la réponse à tous les problèmes :
```
RTFM = Read The Fucking Manual
```
