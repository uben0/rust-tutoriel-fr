# Déplacé ou copié

## Déplacé et non copié

Créons deux variables en en copiant une.
```rust
let a = 23;
let b = a;

println!("{} {}", a, b);
```

Maintenant faisons la même chose mais avec notre structure `Point`.
```rust
let p1 = Point { x: 10.0, y: 7.0 };
let p2 = p1;

println!("{:?} {:?}", p1, p2); // ERROR
```
Le compilateur se plein que `p1` a été déplacée. Effectivement, par défaut, un type ne peut pas être copié, il est déplacé. La valeur stocké dans la variable `p1` est déplacé vers la variable `p2`.

Par exemple, quand on construit une structure `Cercle`:
```rust
let p: Point = Point { x: 10.0, y: 7.0 };
let c: Cercle = Cercle { centre: p, rayon: 2.0 }; // 'p' est déplacé

println!("{:?}", p); // ERROR
```
Ici, la valeur de `p` est utilisé pour construire la valeur de `c`, donc la valeur de type `Point` a été déplacé à l'intérieur de la valeur de type `Cercle`.

## Copié et non déplacé

On a vu que quand on utilise une valeur du type `u32`, `i32`, `bool`, `char` et pleins d'autres, cette dernière est copiée et non déplacée.
```rust
let a: i32 = 10;
let b = a;
println!("{} {}", a, b);
```

Alors pourquoi dans certains cas, la valeur est copiée et dans d'autres déplacée ?

La capacité d'un type à se cloner est décrit par un `trait` appelé `Clone`. Nous n'avons pas encore vu les `trait`s, c'est un sujet crucial mais il est encore trop tôt pour en parler, pour l'instant, il faut juste comprendre qu'un `trait` est une abilité qu'un type peut avoir, comme par exemple se cloner, qui est décrit par le trait `Clone` définit dans la bibliothèque standard.

La plupart des types peuvent être clonés mais peu peuvent être copiés. C'est quoi la différence entre cloner et copier ? L'idée est que l'on califie un type de copiable quand le cloner ne consiste qu'à ne copier quelques octets et que ça n'engendre pas d'opéraitions tiers comme de l'alocation de mémoire. Par exemple, les entiers sont copiables mais une `String` ne l'est pas car la cloner entraine une allocation de mémoire dynamique.

La capacité d'un type à être copié est décrit par le trait `Copy`, qui lui aussi est definit dans la bibliothèque standard.

## Dérivation

Pour faire en sorte que nos structure `Point` et `Cercle` soit clonables et copiables on peut dériver les traits `Clone` et `Copy` sur eux grâce à la directive `#[derive(...)]`.
```rust
#[derive(Debug, Clone, Copy)]
struct Point {
	x: f64,
	y: f64,
}

#[derive(Debug, Clone, Copy)]
struct Cercle {
	centre: Point,
	rayon: f64,
}
```
Et oui, `Debug` est aussi un `trait` qui décrit une abilité qu'a un type, qui est de s'afficher en détails dans un terminal.

Maintenant, `Point` et `Cercle` seront copiés et non déplacés, à la façon des entiers.
```rust
let p1 = Point { x: 10.0, y: 7.0 };
let p2 = p1;

println!("{:?} {:?}", p1, p2);
```
Pratique !