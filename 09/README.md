# Bibliothèque

Beaucoup de types, de fonctions et d'autres éléments sond fournits de base avec le langage, on appelle ça la bibliothèque standard. Cependant, même ci cette dernière est très fournie, on est amené à un moment ou un autre à utiliser des bibliothèques externes. Heuresement, en Rust, utiliser une bibliothèque est très facile.

## Ajouter une biblihothèque
On va utiliser la bibliothèque `rand`, cette dernière permet de générer des valeurs aléatoires. On utilisera la version `0.7` de cette dernière.

Ouvrons le fichier `Cargo.toml`. Il devrait ressembler à ça :
```ini
[package]
name = "hello-world"
version = "0.1.0"
authors = ["user <user@address.com>"]
edition = "2018"

[dependencies]
```
Il suffit de rajouter sous `[dependencies]` la ligne `rand = "0.7"`, comme ceci :
```ini
[package]
name = "hello-world"
version = "0.1.0"
authors = ["user <user@address.com>"]
edition = "2018"

[dependencies]
rand = "0.7"
```
Et voilà, c'est fait ! (et oui, c'est aussi simple que ça)

On peu maintenant générer des valeurs aléatoires :
```rust
let entier_aleatoir: u32 = rand::random();
println!("{}", entier_aleatoir);

let booleen_aleatoir: bool = rand::random();
println!("{}", booleen_aleatoir);
```

Les éléments d'une bibliothèque sont accessibles via une sorte de dossier du même nom, ici `rand::`, donc si on avait utilisé la bibliothèque `log` on aurait noté `log::`.

Notons le même phénomène qu'avec la fonction `user_input`, la fonction `random` semble pouvoir retourner différents types. Patience, j'expliquerai bientôt ce qui se cache derrière cette magie noir.

## Répertoire officiel

Pour voir toutes les biblihothèques disponibles, il faut se rendre sur [https://crates.io](https://crates.io). Par ailleur, les biblihothèques en Rust sont appelés des `crate`s.


## Bibliothèque standard

Il est possible d'explorer la bibliothèque standard en consultant sa documentation sur [https://doc.rust-lang.org/std/](https://doc.rust-lang.org/std/).

Sans connexion internet, si la command `rustup` est installé, on peut ouvrir la doc avec :
```bash
rustup doc --std
```

## Documentation

La documentation des bibliothèques est normalement accessible sur [https://crates.io](https://crates.io).
