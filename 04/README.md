# Variables

## Déclaration d'une variable

```rust
let ma_variable: i32 = 0;
```
On a déclaré une variable appelée `ma_variable` de type `i32` ayant la valeur initial de `0`.

## Mutabilité
```rust
let ma_variable: i32 = 0;
ma_variable = 1; // ERROR
```
Par défaut, une variable n'est pas modifiable, on dit qu'elle n'est pas mutable. On peut corriger cela avec le mot clé `mut`.
```rust
let mut ma_variable: i32 = 0;
ma_variable = 1; // OK
```

## Nomenclature

```rust
let maVariable: i32 = 0; // WARNING
```
Les variables doivent être nomé en *snake_case*. Si une autre convention est utilisée comme le *camelCase*, un avertissement est émit par le compilateur.

## Redéfinition

```rust
let a: i32 = 0;
let a: bool = false;
```
Il est possible de déclarer une variable avec le même nom qu'une déjà déclarée avant, cela a pour effet de masquer la variable précédente.