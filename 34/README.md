# Enveloper un itérateur

L'itérateur `Fibonacci` n'a pas de limite, il produira des valeurs indéfiniment, on aimerai créer un outil qui puisse limiter un itérateur.

Pour cela, on va créer un itérateur générique avec un compteur.
```rust
struct Limite<T> {
    iter: T,
    restant: usize,
}
```
On implémente `Iterateur` sur `Limite`.
```rust
impl Iterator for Limite {
    //            ^^^^^^ERROR
    type Item = ???;
    //          ^^^PROBLEME

    fn next(self: &mut Limite) -> Option<Self::Item> {
        //             ^^^^^^ERROR
        if self.restant != 0 {
            self.restant -= 1;
            self.iter.next()
        }
        else {None}
    }
}
```
Il y a deux problèmes. Que doit-on mettre comme type de retour (`Item`) ? Et le compilateur se plein que l'on a pas spécifier d'argumments sur `Limite`. En effet, `Limite` est générique, elle prend un argument qui est le type de l'itérateur envelopé. Pour pouvoir lui passer un argument, il faudrait déjà en avoir un, pour cela, il faut que l'implémentation de `Iterator` sur `Limite` soit générique elle même.
```rust
impl<T> Iterator for Limite<T> {
    type Item = T::Item;
    //          ^^^^^^^ERROR

    fn next(self: &mut Limite<T>) -> Option<Self::Item> {
        if self.restant != 0 {
            self.restant -= 1;
            self.iter.next()
            //        ^^^^ERROR
        }
        else {None}
    }
}
```
Le compilateur n'est toujours pas content, il se plein qu'il n'a pas trouvé le type `Item` associé à `T` et qu'il n'a pas non plus trouvé la méthode `next` de `T`. Pourtant, `T` est un itérateur et devrait avoir ces deux éléments. Sauf qu'à aucun moment nous n'avons spécifié que `T` était un itérateur. Il faut rajouter une contrainte sur le type `T`.
```rust
impl<T: Iterator> Iterator for Limite<T> {
    type Item = T::Item;

    fn next(self: &mut Limite<T>) -> Option<Self::Item> {
        if self.restant != 0 {
            self.restant -= 1;
            self.iter.next()
        }
        else {None}
    }
}
```
Enfin ! La contrainte stipule que cette implémentation de `Iterator` pour `Limite` ne se fait que si `T` implémente `Iterator` lui même.

Testons-le !
```rust
let fib = Fibonacci::new();
let limite = Limite{iter: fib, restant: 8};
for n in limite {
    println!("{}", n);
}
```
```
0
1
1
2
3
5
8
13
```

Cet outil existe déjà, on peut l'utiliser comme suit.
```rust
let fib = Fibonacci::new();
for n in fib.take(8) {
    println!("{}", n);
}
```
```
0
1
1
2
3
5
8
13
```
Je présente dans le chapitre suivant quelques uns de ces outils.
