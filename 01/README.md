# Introduction

Le langage Rust se veut être le successeur du langage C, une sorte de version moderne de ce dernier. La raison pour la quelle j'apprécie ce langage est qu'il est cohérant, c'est à dire que les règles selon lesquelles est construit le langage n'ont quasiment jamais d'exceptions.

- Le langage Rust est très performant, il est parmi les plus performant.
- C'est un langage compilé, comme le C.
- Il garantie qu'il n'y a pas de bugs.
- Il est très facile d'utiliser des bibliothèques.
- Plusieurs outils sont fourni avec, comme un gestionnaire de projet qui gère la compilation, les bibliothèques, la documentation et les tests, rien que ça.
- Une bibliothèque standard iréprochable.
- Une documentation claire et complète.
- Un program Rust est indépendant de la plateforme. Un projet compilera sous Linux, Windows ou MacOS sans aucun soucis.
- Il permet d'utiliser des techniques tel que le multithreading ou la programmation asynchrone (qui sont amené à être omniprésent dans le future), tout en garantissant l'absence de bug.

## Hello World

En C:
```cpp
int main() {
	printf("Hello World!\n");
	return 0;
}
```

En Rust:
```rust
fn main() {
	println!("Hello World!");
}
```

## Déclaration des variables

En C:
```cpp
int a = 0;
```
En Rust:
```rust
let a: i32 = 0;
```

## Déclaration des fonctions

En C:
```cpp
int add(int a, int b) {
	return a + b;
}
```
En Rust:
```rust
fn add(a: i32, b: i32) -> i32 {
	return a + b;
}
```

## Les types
Voici une correspondance aproxymative des types entre Rust et C:

| Rust | C                  |
|------|--------------------|
|`i8`  |`char`              |
|`i16` |`short int`         |
|`i32` |`int`               |
|`i64` |`long int`          |
|`u8`  |`unsigned char`     |
|`u16` |`unsigned short int`|
|`u32` |`unsigned int`      |
|`u64` |`unsigned long int` |
|`f32` |`float`             |
|`f64` |`double`            |
|`bool`|`bool`              |
|`char`|`char`              |
|`()`  |`void`              |

Il y a bien plus de types, mais ils seront couverts par la suite, par exemple, les tableaux, les références (pointeurs), les énumérations ...

## Installation

Le plus propre pour intsaller est d'utiliser son gestionaire de paquet. Sous *Debian* et *Ubuntu*, avec `apt` :

```bash
sudo apt install cargo rust
```

Sinon, il est possible d'installer manuellement, grâce au script fourni sur le site officielle du language :

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```