# Paternes réfutables

On a vu qu'il était possible de déconstruire à l'aide des paternes, mais aucun des paternes que l'on a vu n'était réfutable.

## Paterne énumération

On a vu comment déconstruire les tuples, les tableaux et les structures mais pas comment déconstruire une énumération, alors essayons.

On imagine une énumération représentant un événement dans une fenêtre et une fonction qui retourne le prochain événement :
```rust
enum Event {
    Quit,
    Click (i32, i32),
    Keyboard { key: char, shift: bool },
}

fn wait_next_event() -> Event;
```

```rust
let Event::Click(x, y) = wait_next_event(); // ERROR
```
On souhaite récupérer les coordonnés du clic mais le compilateur se pleins que notre paterne est réfutable. Effectivement, qu'est-il sensé se passer si jamais la valeur retourné n'était pas la variante `Event::Click` mais par exemple la variante `Event::Keyboard` ? Le paterne serait réfuté, les variables `x` et `y` ne serait pas initialisées, et ce n'est pas autorisé.

## L'embranchement `if let`

Il existe une notation `if let` qui permet d'utiliser des paternes réfutables.
```rust
if let Event::Click(x, y) = wait_next_event() {
    println!("Un clic de souris en x={} et y={}.", x, y);
}
```
Le code dans le `if let` ne s'éxécute que si le paterne a était accepté.

Reprenons notre énumération `JourSemaine` et imaginons une fonction qui affiche un message en fonction du jour passé.
```rust
enum JourSemaine {
	Lundi,
	Mardi,
	Mercredi,
	Jeudi,
	Vendredi,
	Samedi,
	Dimanche,
}

fn message_jour(j: JourSemaine) {
    if let JourSemaine::Lundi = j {
        println!("Je n'irai pas travailler");
    }
    else if let JourSemaine::Mardi = j {
        println!("Je prépare des crèpes");
    }
    else if let JourSemaine::Mercredi = j {
        println!("Je fais la sieste");
    }
    else if let JourSemaine::Jeudi = j {
        println!("C'est manif !");
    }
    else if let JourSemaine::Vendredi = j {
        println!("Le weekend, que c'est bon.");
    }
    else if let JourSemaine::Samedi = j {
        println!("Gilets jaunes");
    }
    else if let JourSemaine::Dimanche = j {
        println!("Je reste sous la couette");
    }
}
```

## Literal

Un paterne peut être un litéral.

```rust
let n: i32 = user_input();

if let 9 = n {
    println!("n == 9");
}
```

Ici, on aurait simplement pu tester `n == 9` mais l'interêt est de combiner la techinque.

```rust
let n: i32 = user_input();
let t = (n, true, 'f');

if let (9, b, c) = t {
    println!("n == 9");
}
```

## Ignorer

Un paterne peut être une ignoration.
```rust
let _ = 0;
```
L'underscore agit comme le nom d'une variable que l'on n'utilisera jamais, une variable poubelle.

## Ou

Un paterne peut être plusieur variante en une.
```rust
if let 2 | 3 | 5 | 7 = n {
    println!("nombre premier");
}
```

## Fourchette

Un paterne peut être une fourchette (un `range` inclusif).
```rust
if let 5..=8 = n {
    println!("compris entre 5 et 8 inclu");
}
```
