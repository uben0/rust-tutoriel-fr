# Créer un itérateur

On a évoqué les itérateurs précédemment. Mais qu'est-ce que c'est ?

Un itérateur est simplement un type qui implémente le trait `Iterator` définit dans la bibliothèque standard. Ce dernier décrit la capacité qu'a un type à itérer sur un ensemble de valeur.

Par exemple, un `range` est un type qui implémente le trait `Iterator`, et itère sur l'ensemble des valeurs comprises dans ce `range`.

## Créons un itérateur

Créons une struture `Intervalle` qui implémente `Iterator`.
```rust
struct Intervalle {
    debut: usize,
    fin: usize,
}
```
Avant d'implémenter `Iterator`, jetons un cou d'oeuil à sa définition.
```rust
trait Iterator {
    type Item;
    fn next(&mut self) -> Option<Self::Item>;
}
```
On observe qu'il n'y a pas seulement un prototype de fonction mais aussi un alias de type. L'alias doit être associé au type des éléments retourné par l'itération.

Par exemple, si on itérait sur une chaîne de caractères, on aurait `type Item = char;`.

Ici, on itère sur un intervalle de `usize`.
```rust
impl Iterator for Intervalle {

    type Item = usize;

    fn next(self: &mut Intervalle) -> Option<usize> {
        if self.debut < self.fin {
            let resultat = self.debut;
            self.debut += 1;
            Some(resultat)
        }
        else {
            None
        }
    }
}
```

## Utilisons notre itérateur

```rust
let mut intervalle = Intervalle{debut: 6, fin: 9};

println!("{:?}", intervalle.next());
println!("{:?}", intervalle.next());
println!("{:?}", intervalle.next());
println!("{:?}", intervalle.next());
println!("{:?}", intervalle.next());
```
```
Some(6)
Some(7)
Some(8)
None
None
```
Une fois que la fin est ateinte, il ne retourne que `None`.

## Fibonacci

Les itérateurs sont très versatiles, il sont omniprésents et très pratiques. Pour avoir un aperçu des possibilités, on va créer un itérateur sur la suite de Fibonacci.

D'abord reprenons notre fonction `fibonacci` vu précédement.
```rust
fn fibonacci(n: usize) -> u128 {
    match n {
        0 => 0,
        1 => 1,
        x => fibonacci(n - 1) + fibonacci(n - 2)
    }
}
```

Notre fonction `fibonacci` est élégante mais sa complexité est affreuse, essaye de l'appeler avec `fibonacci(50)`, c'est long, très long... Pourtant, elle peut être calculée plus rapidement.

```rust
fn fibonacci(n: usize) -> u128 {
    let mut a = 0;
    let mut b = 1;
    for _ in 0..n {
        let tmp = a + b;
        a = b;
        b = tmp;
    }
    a
}
```

Cette fonction est rapide mais elle n'est pas versatile. Par exemple, si je veux les n-premiers éléments de la suite, je vais devoir l'appeler n-fois, ce n'est pas optimisé.

Les itérateurs sont une réponse élégante à ce problème.
```rust
struct Fibonacci {
    a: u128,
    b: u128,
}

impl Iterator for Fibonacci {
    type Item = u128;
    fn next(self: &mut Fibonacci) -> Option<u128> {
        let a = self.a;
        let b = self.b;

        self.a = b;
        self.b = a + b;

        Some(a)
    }
}
```

Utilisons-le.
```rust
let mut fib = Fibonacci{a: 0, b: 1};

println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
println!("{:?}", fib.next());
```
```
Some(0)
Some(1)
Some(1)
Some(2)
Some(3)
```

Ajoutons un constructeur à `Fibonacci`.
```rust
impl Fibonacci {
    fn new() -> Fibonacci {
        Fibonacci{a: 0, b: 1}
    }
}
```
