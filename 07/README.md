# Embranchements

## Branche conditionnelle `if else`

```rust
let a: u32 = 42;
let b: u32 = 41;

if a > b {
	println!("{} est plus grand que {}", a, b);
}
```
Notons qu'il n'y a pas besoins de parenthèses autour de la condition comme en C où l'on aurait `if (a > b)`.

Ajoutons un `else`:
```rust
let a: u32 = 42;
let b: u32 = 41;

if a > b {
	println!("{} est plus grand que {}", a, b);
}
else {
	println!("{} n'est pas plus grand que {}", a, b);
}
```

Ajoutons un `else if`:

```rust
let a: u32 = 42;
let b: u32 = 41;

if a > b {
	println!("{} est plus grand que {}", a, b);
}
else if a < b {
	println!("{} est plus petit que {}", a, b);
}
else {
	println!("{} est égal à {}", a, b);
}
```

## Boucle `while`

```rust
let mut i: u32 = 0;

while i < 5 {
	println!("{}", i);
	i = i + 1;
}
```
Cette boucle affiche les nombres de `0` à `4`. Notons le mot clé `mut` dans la déclaration de `i`, qui est nécessaire pour rendre cette variable mutable, sans lui, on ne pourrait pas incrémenter `i`.

Comm en C, l'instruction `break` permet de sortir directement d'une boucle, et l'instruction `continue` permet de revenir au début:
```rust
let mut i: u32 = 1;
while i < 10 {
	i = i + 1;

	if i == 5 {
		continue
	}

	if i == 7 {
		break
	}
}
```

## Variable booléenne
Une condition est simplement une valeur de type `bool`, donc il est possible de stocker l'évaluation d'une condition dans une variable de type `bool` et l'utiliser ensuite dans un `if` ou un `while`.

```rust
let condition: bool = 41 > 35 && 16 < 35;

if condition {
	println!("hello");
}
```
Il n'est pas possible d'utiliser un entier en tant que booléen comme en *C*:
```rust
if 43 % 2 { // ERROR
	println!("43 n'est pas pair");
}
```
Il faut tester explicitement qu'il ne soit pas égale à `0`:
```rust
if (43 % 2) != 0 { // OK
	println!("43 n'est pas pair");
}
```