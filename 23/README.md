# Les énumérations

Imaginons que l'on veuille stocker les jours de la semaines. On pourrait simplement utiliser un entier `u8` avec les valeurs `0`, `1`, `2`, `3`, `4`, `5` et `6` associés aux jours de la semaine. Mais cette façon de faire comporte des désavantages, par exmple, on pourrait se tromper et rentrer une valeur qui n'aurait pas de sens comme `7` ou n'importe quelle autre valeur n'étant pas comprise entre `0` et `6` inclu.

Heuresement, il y a les énumérations. Créons-en une pour les jours de la semaine:
```rust
#[derive(Debug, Clone, Copy)]
enum JourSemaine {
	Lundi,
	Mardi,
	Mercredi,
	Jeudi,
	Vendredi,
	Samedi,
	Dimanche,
}
```
Déclarons une variable de ce type et affichons la:
```rust
let jour: JourSemaine = JourSemaine::Mercredi;
println!("{:?}", jour);
```
```
Mercredi
```

## Stocker des valeurs

On peut stocker des valeurs en fonction de la variante de l'énumération:
```rust
#[derive(Debug, Clone, Copy)]
enum Event {
	Quit,
	Click (i32, i32),
	KeyBoard { key: char, shift: bool },
}
```
On peut imaginer une fonction renvoyant des événements se produisant sur une fenêtre :
```rust
fn wait_next_event() -> Event;
```
```rust
let event: Event = wait_next_event();
println!("{:?}", event);
```
```
Click(128, 87)
```

## Une variante, une structre

En fait, chaque variante d'une énumération est une structure. Quand on n'associe aucune valeur à une variante (comme les jours de la semaine), ce sont des structures vides (et oui, elles ont leur utilité).

## Plus à savoir

Les énumérations sont très pratiques, mais là où on en est, nous n'avons pas les outils pour exploiter leur plein potentiel.