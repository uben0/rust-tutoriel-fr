# S'affranchir des règles

Nous avons vu que les règles strictes en Rust sont très restrictives. Notament les règles d'appartenance et de duré de vie. C'est ces denières qui empêchent d'allouer de la mémoire dynamiquement et de l'utiliser car il n'y a aucun moyen de déterminer son apartenance ni sa duré de vie (elle n'en a pas vraiment, puisqu'elle est dynamique).

## Mot clé `unsafe`

Ne pas pouvoir utiliser de la mémoire dynamiquement allouée serait trop restrictif et c'est pour cela qu'il existe un mot clé `unsafe` qui permet de passer outre certaines règles.

Il existe dans la bibliothèque standard une fonction qui permet d'allouer dynamiquement de la mémoire.
```rust
unsafe fn alloc(layout: Layout) -> *mut u8
```
Cette dernière prend une valeur de type `Layout` qui est pour faire simple la taille du segment que l'on veut allouer et retourne un **pointeur** sur un octet, oui, tu m'as bien entendu, il s'agit d'un **pointeur** (`*u8`) et non d'une **référence** (`&u8`). Une valeur de type pointeur (`*`) peut être créé sans avoir à respecter d'apartenance ni de duré de vie, il peut même pointer n'importe où, c'est comme un pointeur en C. Ici, le type indique qu'il pointe sur un `u8` mais c'est simplement car la fonction `alloc` ignore ce pour quoi ce segment va servir.

## Block `unsafe`

Appelons `alloc` pour allouer un segment pouvant stocker un `i32`.
```rust
use std::alloc::{alloc, Layout};

let a: *mut u8 = alloc(Layout::new::<i32>()); // ERROR
```
Le compilateur se pleins. Effectivement, ce qui se passe ici est qualifié de `unsafe`, le compilateur ne peux plus garantir que l'on ne fait pas d'erreur. Il faut indiquer au compilateur que l'on souhaite prendre le risque en utilisant un block `unsafe`.
```rust
use std::alloc::{alloc, Layout};

let a: *mut u8 = unsafe {
    alloc(Layout::new::<i32>())
};
```

## Ne jamais utiliser `unsafe`

Heuresement, nous n'auront jamais à utiliser `unsafe` car il existe déjà dans la bibliothèque standard des types et des fonctions qui donnent accès à des fonctionnalités `unsafe` en utilisant `unsafe` uniquement en **interne**, ça signifie que l'on a pas à se soucier de quoi que ce soit.

## Pointeur unique

Par exemple, si l'on souhaite créer un arbre binaire.
```rust
enum BinTree { // ERROR
    Leaf,
    Node(BinTree, BinTree),
}
```
Cette structure de donné est récursive, elle se contient elle même, donc sa taille devrait être infinie. Pour résoudre ce problème, on peut utiliser l'allocation dynamique de sorte à ce que les éléments récursifs ne soit pas stockés dans la structure elle-même.
```rust
enum BinTree {
    Leaf,
    Node(Box<BinTree>, Box<BinTree>),
}
```
Le type `Box` représente un pointeur unique, c'est à dire que le segment pointé a la même duré de vie et apartenance que le pointeur.
```rust
let a: Box<i32> = Box::new(42);
let b: i32 = 34 + *a;
```
Jettes un oeuil à la documentation.

## Tableau dynamique
Le type `Vec` représente un tableau dont la taille est dynamique.
```rust
let mut a = Vec::new();
a.push(42);
a.push(34);
```
Ce type alloue un segment en fonction du besoins de stockage. Jettes un oeuil à la documentation.

Le type `String` est quasi équivalent à un `Vec<u8>`. Depuis la documentation, tu peux accèder au code source, et voir comment est définit la structure `String` (à droite il y a `[src]`, clique dessus).
```rust
pub struct String {
    vec: Vec<u8>,
}
```

## Échanger

Pour échanger deux entiers, c'est facile.
```rust
fn echange_i32(a: &mut i32, b: &mut i32) {
    let tmp = *a;
    *a = *b;
    *b = tmp;
}
```
Mais échanger des valeurs qui ne sont pas copiables est une autre histoire.
```rust
fn echange<T>(a: &mut T, b: &mut T) {
    let tmp = *a; // ERROR
    *a = *b; // ERROR
    *b = tmp;
}
```
On doit extraire la valeur de `a` et de `b`. On ne peut pas faire ça selon les règles d'appartenance en Rust. Mais une fonction `swap` dans la bibliothèque standard le fait.
```rust
fn echange<T>(a: &mut T, b: &mut T) {
    std::mem::swap(a, b);
}
```

## Appels systèmes
Les appels systèmes ne sont pas du tout sécurisé, il ne sont donc pas directement accessibles.

Pour ouvrir un fichier, il existe le type `File`.
```rust
use std::fs::File;
let file = File::open("hello.txt").unwrap();
```

Pour ouvrir une socket, il existe les types `UdpSocket`, `TcpStream` et `TcpListener`.
```rust
use std::net::UdpSocket;
let mut socket = UdpSocket::bind("127.0.0.1:34254").unwrap();
```

## Conclusion
Il n'y a pas besoins d'utiliser le mot clé `unsafe`, et de toute façon il ne faut pas, car il existe déjà des types et fonctions qui le font pour toi de façon sécurisée. C'est comme s'il y avait deux mondes: le monde sécurisé et le monde dangereux. Tant que tu n'utilises pas le mot clé `unsafe`, tu restes du coté sécurisé. Il existe des fonctions et types dans la bibliothèque standard qui s'aventure dans le monde dangereux sans que tu ais à le faire toi même, un peu comme si tu demandait à un paladin en armure d'aller chercher quelque chose pour toi dans la forêt maudite.
