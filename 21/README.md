# Méthodes

Nous avons écrit précédemment les fonctions `deplace_point` et `air_cercle`, qui toutes deux sont logiquement liées aux types `Point` et `Cercle`. Il y a une façon de créer des fonctions associées avec le mot clé `impl`.

```rust
impl Point {
	fn deplace(p: &mut Point, dx: f64, dy: f64) {
		p.x += dx;
		p.y += dy;
	}
}
```
```rust
let mut p = Point { x: 2.0, y: 3.0 };
Point::deplace(&mut p, 2.0, 1.0);
println!("{:?}", p);
```
```
Point { x: 4.0, y: 4.0 }
```

On voit que la fonction `deplace` est rangée dans une sorte de dossier nomé `Point::`.

## Constructeur

Il n'y a pas de constructeurs en Rust, mais par convention, pour construire un `type`, on lui associe une fonction nomée `new`, rien n'oblige à l'appeler comme ça, `new` n'est pas un mot clé, c'est juste une convention.
```rust
impl Point {
	fn new(x: f64, y: f64) -> Point {
		let p = Point { x: x, y: y };
		return p;
	}
	fn deplace(p: &mut Point, dx: f64, dy: f64) {
		p.x += dx;
		p.y += dy;
	}
}
```
```rust
let p1 = Point { x: 2.0, y: 3.0 };
let p2 = Point::new(2.0, 3.0);
```
Les deux initialisations sont équivalantes, et ici la fonction `new` n'apporte pas une simplification notable, mais dans le cas de structures plus complexes, il vaut mieux avoir une fonction `new`.

## Encapsulation
Rust n'est pas un langage orienté objet mais beaucoup de concepts de programmation orienté objets se retrouve dans Rust, comme le concept d'encapsulation qui consiste à rendre un type opaque et limiter son utilisation via des fonctions. Par exemple, ne pouvoir construire un type que en utilisant un constructeur. On vera plus tard comment encapsuler un type dans les règles de l'art. Pour l'instant nous n'avons pas les outils pour le faire.

## Méthodes
Ce serait pratique de pouvoir écrire `Point::deplace` comme si c'était une méthode de `Point` et pas juste une fonction associée.
```rust
let mut p = Point::new(2.0, 3.0);
p.deplace(1.0, 0.5); // ERROR
```
C'est possible, pour cela il suffit de modifier la définition de `deplace` en remplaçant le nom de la variable représentant l'objet par le mot clé `self` :
```rust
impl Point {
	fn deplace(self: &mut Point, dx: f64, dy: f64) {
		self.x += dx;
		self.y += dy;
	}
}
```
De cette façon, le compilateur comprend que l'on souhaite que la fonction `deplace` puisse être utilisée comme une méthode.
```rust
let mut p = Point::new(2.0, 3.0);
p.deplace(1.0, 0.5); // OK
```

Ajoutons une méthode qui retourne la distance avec un autre point:
```rust
fn distance(self: &Point, p: &Point) -> f64 {
	let dx = self.x - p.x;
	let dy = self.y - p.y;
	return f64::sqrt(dx * dx + dy * dy);
}
```
```rust
let a = Point::new(2.0, 3.0);
let b = Point::new(10.0, 7.0);

let d = a.distance(&b);
println!("{}", d);
```
```
8.94427190999916
```
