fn main() {
	let mut tableau: [u32; 10] = [0; 10];
	for i in 0..10 {
		let aleatoir: u32 = rand::random();
		tableau[i] = aleatoir % 100;
	}
	println!("{:?}", tableau);
	for i in 0..10 {
		let mut minimum = i;
		for j in i..10 {
			if tableau[j] < tableau[minimum] {
				minimum = j;
			}
		}
		let temporair = tableau[i];
		tableau[i] = tableau[minimum];
		tableau[minimum] = temporair;
	}
	println!("{:?}", tableau);
}