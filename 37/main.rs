#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum PieceType {
    King,
    Queen,
    Rook,
    Knight,
    Bishop,
    Pawn,
}
use PieceType::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Color {
    Black,
    White,
}
use Color::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Piece {
    t: PieceType,
    c: Color,
}
impl Piece {
    fn to_unicode(self: Piece) -> char {
        match (self.c, self.t) {
            (Black, King  ) => '♚',
            (Black, Queen ) => '♛',
            (Black, Rook  ) => '♜',
            (Black, Knight) => '♞',
            (Black, Bishop) => '♝',
            (Black, Pawn  ) => '♟',
            (White, King  ) => '♔',
            (White, Queen ) => '♕',
            (White, Rook  ) => '♖',
            (White, Knight) => '♘',
            (White, Bishop) => '♗',
            (White, Pawn  ) => '♙',
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Game {
    board: [[Option<Piece>; 8]; 8],
}
impl Game {
    fn new() -> Game {
        let mut g = Game {
            board: [[None; 8]; 8],
        };

        let line = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook];

        for x in 0..8 {
            g.board[0][x] = Some(Piece{t: line[x], c: Black});
            g.board[1][x] = Some(Piece{t: Pawn,    c: Black});
            g.board[6][x] = Some(Piece{t: Pawn,    c: White});
            g.board[7][x] = Some(Piece{t: line[x], c: White});
        }

        return g;
    }
    fn print(self: &Game) {
        for y in 0..8 {
            for x in 0..8 {
                match self.board[y][x] {
                    Some(piece) => {
                        print!("{} ", piece.to_unicode());
                    }
                    None => {
                        print!("  ");
                    }
                }
            }
            println!("");
        }
    }
}

fn main() {
    let g = Game::new();
    g.print();
}
