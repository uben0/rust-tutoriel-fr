# Les traits

Le concept de `trait` est le plus important en Rust, tout s'articule autour. Le concept de `trait` permet de coder comme si le langage Rust était un langage de haut niveau sans qu'il le soit.

Un `trait` représente une **abilité** que certains types peuvent avoir.

Par exemple, l'entré standard (STDIN), les fichiers, les sockets, et d'autres, sont des types dont on peut lire des octets. On aimerai avoir une abstraction sur cette propriété, de cette façon, des bouts de code pourraient manipuler des types inconnus mais dont on sait qu'on peut lire des octets:
```rust
fn read_and_print(input: &mut dyn Read) {
	let mut buffer = [0u8; 10];
	input.read(&mut buffer).unwrap();
	println!("{:?}", buffer);
}
```
Cette fonction lit jusqu'à `10` octets et affiche le `buffer`.

Elle prend une référence vers le type `dyn Read`, ça signifie que le type est inconnu mais on sait qu'il implémente le trait `Read` définit comme suit :
```rust
trait Read {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize>;
}
```
Le trait `Read` est définit dans la bibliothèque standard.

On peut voir un `trait` comme une interface standardisé que des types peuvent adopter et ainsi permettre à des bouts de codes de les utiliser sans avoir à connaître leur type.

## Créons un trait

Reprenons les structures `Point` et `Cercle`, et déclarons d'autres formes tel que le rectangle et le triangle:

```rust
#[derive(Debug, Clone, Copy)]
struct Point {
	x: f64,
	y: f64,
}

#[derive(Debug, Clone, Copy)]
struct Cercle {
	centre: Point,
	rayon: f64,
}

#[derive(Debug, Clone, Copy)]
struct Rectangle {
	centre: Point,
	largeur: f64,
	hauteur: f64,
}

#[derive(Debug, Clone, Copy)]
struct Triangle {
	a: Point,
	b: Point,
	c: Point,
}
```
On aimerait pouvoir intéragir sans distinction entre eux quand on ne s'intéresse qu'à l'idée de forme, par exemple, une forme peut-être déplacé, elle a une aire, un périmetre, elle peut être dessinée, etc...

Déclarons le trait `Forme` décrivant comment intéragir avec un type représentant une forme.
```rust
trait Forme {
	fn deplace(self: &mut Self, dx: f64, dy: f64);
	fn aire(self: &Self) -> f64;
	fn perimetre(self: &Self) -> f64;
}
```
On peut voir des prototypes de fonction, elle n'ont pas de corps. Un `trait` explique comment intéragir avec une valeur de type inconnu, à partir du moment où elle implémente ce `trait`. Cette intéraction se fait à travers des appels de fonction. On remarque que le type de `self` est une référence sur `Self`, qui est le type inconnu en question.

## Implémentons notre trait

Implémentons `Forme` sur `Cercle`:
```rust
impl Forme for Cercle {
	fn deplace(self: &mut Cercle, dx: f64, dy: f64) {
		self.centre.deplace(dx, dy);
	}
	fn aire(self: &Cercle) -> f64 {
		return self.rayon * self.rayon * 3.14159;
	}
	fn perimetre(self: &Cercle) -> f64 {
		return 2.0 * self.rayon * 3.14159;
	}
}
```
On reprend les prototypes de fonction dans la défénition de `Forme` et on leur écrit un corps correspondant. On peut maintenant utiliser ces fonctions sur `Cercle` tel des méthodes:
```rust
let mut c = Cercle {
	centre: Point { x: 0.0, y: 1.0 },
	rayon: 4.0,
};
c.deplace(3.0, 2.0);
println!("aire: {}", c.aire());
println!("perimetre: {}", c.perimetre());
```
Mais l'intérêt est d'implémenter un `trait` sur plusieurs types. Alors nous allons implémenter `Forme` sur `Rectangle`:
```rust
impl Forme for Rectangle {
	fn deplace(self: &mut Rectangle, dx: f64, dy: f64) {
		self.centre.deplace(dx, dy);
	}
	fn aire(self: &Rectangle) -> f64 {
		return self.largeur * self.hauteur;
	}
	fn perimetre(self: &Rectangle) -> f64 {
		return 2.0 * (self.largeur + self.hauteur);
	}
}
```
Et on l'implémente sur `Triangle` aussi:
```rust
impl Forme for Triangle {
    fn deplace(self: &mut Triangle, dx: f64, dy: f64) {
        self.a.deplace(dx, dy);
        self.b.deplace(dx, dy);
        self.c.deplace(dx, dy);
    }
    fn aire(self: &Triangle) -> f64 {
        let ab = self.a.distance(&self.b);
        let bc = self.b.distance(&self.c);
        let ca = self.c.distance(&self.a);
        let s = (ab + bc + ca) / 2.0;
        return f64::sqrt(s * (s - ab) * (s - bc) * (s - ca));
    }
    fn perimetre(self: &Triangle) -> f64 {
		return
			self.a.distance(&self.b) +
			self.b.distance(&self.c) +
			self.c.distance(&self.a)
		;
    }
}
```

## Abstraction
Maintenant, imaginons une fonction prenant deux formes et retournant le périmètre de celle qui a la plus grande aire:
```rust
fn perimetre_pga(a: &dyn Forme, b: &dyn Forme) -> f64 {
	if a.aire() > b.aire() {
		return a.perimetre();
	}
	else {
		return b.perimetre();
	}
}
```
Le type `&dyn Forme` est un pointeur lourd, ce pointeur lourd est composé en fait de deux pointeurs différents, un qui pointe vers la variable de type inconnu, et l'autre vers son implémentation du trait `Forme`. Ce que l'on appelle "son implémentation de `Forme`" est une table contenant les fonctions du trait `Forme` implémentées pour ce type inconnu. La fonction `perimetre_pga` n'a pas besoins de connaître le type qui se cache derrière `a` ou `b`, puisque le pointeur lourd donne accès aux fonctions qui permettent d'intéragir avec.
```
 poiteur lourd       valeur de type et de taille inconnu
▄┏━━━━━━━━━┓         ┌─── ─ ─           ─ ─ ───┐
█┃ 0x7f004 ┣━━╺╺╺╺╺╺◆│ self                    │
█┣━━━━━━━━━┫         └─── ─ ─           ─ ─ ───┘
█┃ 0x7f0f6 ┣━┓
▀┗━━━━━━━━━┛ ┃       table avec adresses des fonctions du trait
             ┃       ┏━━━━━━━━━┳━━━━━━━━━┳━━━━━━━━━┓
             ┗╺╺╺╺╺╺◆┃ 0x100f6 ┃ 0x1040b ┃ 0x102c0 ┃
                     ┗━━━━┳━━━━┻━━━━┳━━━━┻━━━━┳━━━━┛
         ┏╺╺╺╺╺╺╺╺╺╺╺╺╺╺╺╺┛  ┏╺╺╺╺╺╺┛         ┗╺╺┓
         ◆                   ◆                   ◆
┌────────────────┐  ┌────────────────┐  ┌────────────────┐
│ fn deplace()   │  │ fn aire()      │  │ fn perimetre() │
╷                ╷  ╷                ╷  ╷                ╷
╷                ╷  ╷                ╷  ╷                ╷
╷                ╷  ╷                ╷  ╷                ╷
└─ ─  ─    ─  ─ ─┘  └─ ─  ─    ─  ─ ─┘  └─ ─  ─    ─  ─ ─┘
```

Pour mieux comprendre ce qui se passe, ajoutons un `println` dans les implémentation de `Forme` par `Cercle` et `Rectangle`:
```rust
impl Forme for Rectangle {
	fn deplace(self: &mut Rectangle, dx: f64, dy: f64) {
		println!("rectangle_deplace");
		self.centre.deplace(dx, dy);
	}
	fn aire(self: &Rectangle) -> f64 {
		println!("rectangle_aire");
		return self.largeur * self.hauteur;
	}
	fn perimetre(self: &Rectangle) -> f64 {
		println!("rectangle_perimetre");
		return 2.0 * (self.largeur + self.hauteur);
	}
}

impl Forme for Cercle {
	fn deplace(self: &mut Cercle, dx: f64, dy: f64) {
		println!("cercle_deplace");
		self.centre.deplace(dx, dy);
	}
	fn aire(self: &Cercle) -> f64 {
		println!("cercle_aire");
		return self.rayon * self.rayon * 3.14159;
	}
	fn perimetre(self: &Cercle) -> f64 {
		println!("cercle_perimetre");
		return 2.0 * self.rayon * 3.14159;
	}
}
```
Maintenant testons `perimetre_pga`:
```rust
let c = Cercle {
	centre: Point { x: 0.0, y: 1.0 },
	rayon: 4.0,
};
let r = Rectangle {
	centre: Point { x: 3.0, y: 2.0},
	largeur: 10.0,
	hauteur: 4.0,
};
let p = perimetre_pga(&c, &r);
println!("{}", p);
```
```
cercle_aire
rectangle_aire
cercle_perimetre
25.13272
```
On obeserve que les fonctions correspondant au type ont été appelées, c'est grâce aux pointeurs lourds `&dyn Forme`.

## Debug, Clone, Copy

La capacité d'un type à s'afficher en détails dans un terminal est décrit par le trait `Debug`. La capacité d'un type à se cloner (se dédoubler) est décrit par le trait `Clone`.

Les traits `Debug`, `Clone` et `Copy` peuvent être implémentés automatiquement pour les structures, on a pas besoins de le faire à la main. On utilise la directive `#[derive()]` pour le faire automatiquement.

Le trait `Copy` est juste un trait marqueur, en fait, il ne nécessite qu'aucune fonction soit implémentée pour qu'un type l'implémente, si ce n'est que ce type doit implémenter le trait `Clone`. Il sert uniquement à indiquer que si un type l'implémente, alors cloner ce type n'est pas une opération couteuse.