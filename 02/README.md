# Compilation

On pourrai utiliser la commande `rustc` (le compilateur Rust) pour compiler un programme, mais il vaut mieux utiliser `cargo`. Pour comparer avec le C, `rustc` correspond à `gcc` (le compilateur) et `cargo` à `make` (le gestionnaire de projet).

Pour créer un nouveau projet nomé `hello-world`, il faut éxécuter la commande:
```bash
cargo new hello-world --vcs none
```
(si on ne met pas `--vcs none`, le répertoir créé sera un répertoir git, hors ici nous n'en avons pas l'utilité)

Un dossier `hello-world` a été créé contenant un projet de base:
```
hello-world/
├─ Cargo.toml
└─ src/
   └─ main.rs
```
Allons dedans:
```bash
cd hello-world
```
Le fichier `Cargo.toml` est l'équivalent du `Makefile` ou `CMake` en C, il contient les informations du projet, comment il doit être compilé, quels bibliothèques il utilise, etc...

Le dossier `src/` contient les fichiers sources, qui en Rust finissent par `.rs`. Le `main.rs` contient un **Hello World** :
```rust
fn main() {
    println!("Hello, world!");
}
```

Pour compiler et lancer le programme, éxécutons :
```bash
cargo run
```
Cette commande va intéligemment comprendre ce qui doit être compilé et lancé. Il devrait s'afficher :
```
Hello, world!
```
Un fichier `Cargo.lock` et un dossier `target/` sont apparus, ils sont créés et utilisés par `cargo`, il n'y a pas besoins de s'en préoccuper.

## Dispute avec le compilateur

En Rust, il y a des règles très strictes, notre code ne compilera pas tant qu'il ne les respectera pas, cela permet de garantir que si notre code compile, alors, c'est qu'il ne contient pas d'erreur. Ce peut être très frustrant au début, de ne pas réussir à satisfaire le compilateur alors que notre code semble valide, mais ce n'est qu'un temps d'adaptation qui en vaut la peine, car quand en C, notre programme compile, on passe généralement 90% de notre temps à corriger des buggs causés par des erreurs d'inattentions. Grâce aux règles strictes en Rust, cela ne se produit pas.

Quand Rust rend impossible la compilation de code car il ne respecte pas certaines règles, on peut avoir l'impression que cela rend impossible la réalisation du projet, que Rust est trop restrictif, mais retenons bien que si Rust ne permet pas quelque chose, c'est qu'il existe une bien meilleur façon de le faire, elle est juste moins évidente.