# Les paternes

Les paternes sont très importants, ils permettent notament d'utiliser correctement les tuples et rendent possible un embranchement appelé `match` qui est très pratique.

Tu as utilisé des paternes sans le savoir, par exemple lors d'une déclaration de variable.
```rust
let a = 0;
```
Le `a` est un paterne, très basic certe, mais un paterne quand même, voyons un autre exemple.

## Paterne tuple

```rust
let v = (345, true, 'a');
```
On va changer `v` par un paterne plus interressant.
```rust
let (a, b, c) = (345, true, 'a');
```
Ici, on déconstruit le tuple pour obtenir trois variables `a`, `b` et `c` qui contiennent respectivement `345`, `true` et `'a'`.

## Paterne tableau

```rust
let t = [4, 6, 1, 9];
```
Changeons `t` par un paterne plus interressant.
```rust
let [a, b, c, d] = [4, 6, 1, 9];
```
On a déconstruit le tableau.

## Paterne structure

Il est possible de destructurer une structure avec un paterne.
```rust
struct Point {
	x: f64,
	y: f64,
}
```
```rust
let p = Point{x: 9.0, y: 3.0};

let Point{x: a, y: b} = p;
```
On récupère les deux champs dans `a` et `b`.

## Combinaisons

Les paternes sont récursif, donc on peut les combiner à l'infini.
```rust
let complex = ((true, 'f'), [3, 4, 5], Point{x: 1.0, y: 2.0});

let ((a, b), [c, d, f], Point{x: g, y: h}) = complex;
```

Et on est pas obligé de destructuré jusqu'au type primitif.
```rust
let complex = ((true, 'f'), [3, 4, 5], Point{x: 1.0, y: 2.0});

let (a, [b, c, d], e) = complex;
```
Ici, on a gardé le tuple `(true, 'f')` dans `a` et le `Point` dans `e`.

## Paternes références
Il est même possible de destructurer une référence, en d'autres termes, la déréférencer.
```rust
let &a = &0;
```
On récupère l'adresse d'une valeur mais le paterne `&a` récupère dans `a` la valeur référée.

## Conclusion
Il faut voir les paternes comme l'inverse de la construction. Tout se qui est construit (tuples, tableaux, références, structures, etc... ) peut-être déconstruit en utilisant la même notation mais dans un paterne.
