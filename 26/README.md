# Chaînes de caractère

On a vu que pour stocker une chaîne de caractère on utilise `String`:
```rust
let nom: String = user_input();
```

Pourtant, si on essaye simplement de lui affecter un literal, ça ne passe pas:
```rust
let nom: String = "Hello"; // ERROR
```
Le compilateur se plein que le type attendu est `String` mais que le type passé est `&str`. Ces deux types sont différents.

En fait `&str` est une référence vers une chaîne de caractères. Le type `str` est quasiment identique à `[u8]` (j'expliquerai la différence, supposons qu'il sont identique pour l'instant), donc il s'agit d'un tableau d'octets. Étant donné que la taille n'ai pas connu par le type, `&str` est un pointeur lourd, il est acompagné de la taille du tableau.

```
poiteur lourd         chaîne de caractère en mémoire
▄┏━━━━━━━━━┓          ┌───┬───┬───┬───┬───┐
█┃ 0x7f004 ┣━╺╺╺╺╺╺╺╺◆│ H │ e │ l │ l │ o │
█┡━━━━━━━━━┩          └───┴───┴───┴───┴───┘
█│       5 │
▀└─────────┘
```

Un `&str` ne permet pas de stocker une chaîne de caractère, seulement d'en pointer une en mémoire. Il ne possède pas cette chaîne. Alors que `String` aloue dynamiquement son propre segment pour stocker une chaîne de charactère.

```rust
let s = String::from("Hello");
```

```
struct String          segment alloué possédé par la structure
▄┏━━━━━━━━━┓          ▄┌───┬───┬───┬───┬───┐ ─ ┬ ─ ┬ ─ ┐
█┃ 0x420f2 ┣━╺╺╺╺╺╺╺╺◆█│ H │ e │ l │ l │ o │
█┡━━━━━━━━━┩          ▀└───┴───┴───┴───┴───┘ ─ ┴ ─ ┴ ─ ┘
█│       8 │ capacité
█├─────────┤
█│       5 │ utilisation
▀└─────────┘
```

Il existe une méthode `as_str` sur `String` qui permet de retourner un pointeur sur la chaîne de caractères.
```rust
let a: String = String::from("Hello");

let b: &str = a.as_str();
```

# Bytes versus Chars

En Rust, un caractère n'est pas un octet (`char` différent de `u8`), un caractère est codé sur 32 bits, c'est un caractère unicode. Les caractères ne sont donc pas limités à la table ASCII:
```rust
let hiragana: char = 'あ';
```
Il est possible d'utiliser des octets à l'ancienne, comme en C.
```rust
let ascii: u8 = b'A';
```
Il faut placer un `b` devant `' '` pour indiquer que c'est un octet, c'est le `b` de `b`yte.

Il existe une méthode `as_bytes` sur les chaîne de caractères pour qu'elles retournes un poiteur sur le tableau d'octets:
```rust
let unicode: &str = "こんにちは世界";
let bytes: &[u8] = unicode.as_bytes();
println!("{:#?}", bytes);
```
```
[
    227,
    129,
    147,
    227,
    130,
    147,
    227,
    129,
    171,
    227,
    129,
    161,
    227,
    129,
    175,
    228,
    184,
    150,
    231,
    149,
    140,
]
```
La raison pour laquelle il existe un type `str` et pas juste `[u8]`, c'est parce que Rust garantie qu'une chaine de caractère est de l'UTF8 valide, hors, avec un `[u8]` on serrait libre de changer les octets comme bon nous semble sans respecter l'encodage UTF8.

Les chaînes de caractère ne sont pas non plus des `[char]` car même si ce serait plus élégant, cela prendrai beaucoup plus de place, chaque caractère ferait 32 bits de large au lieu de 8. Mais surtout ne permetrait pas une interopérabilité facile avec le système.
