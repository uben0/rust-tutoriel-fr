# Alias de type

On peut définir un alias pour un type choisi.
```rust
type Point = (f64, f64);
```

C'est seulement un alias, ça ne définit pas un nouveau type comme on le ferait avec le mot clé `struct`. Ici, `Point` et `(f64, f64)` sont interchangeables :

```rust
let p1: Point = (0.0, 3.0);
let p2: (f64, f64) = (1.0, -2.0);

let (x1, y1) = p1;
let (x2, y2) = p2;
```

Tous les types peuvent être aliassés, sans exception.