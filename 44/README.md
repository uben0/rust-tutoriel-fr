# Jeu d'échecs (6)

Créons une fonction `reachable_by_player` qui indique quelles cases sont atteignables par au moins une des pièces d'un joueur.
```rust
fn reachable_by_player(self: &Game, player: Color, map: &mut [[bool; 8]; 8]) {
	for y in 0..8 {
		for x in 0..8 {
			// pour toutes les pièces du plateau
			if let Some(piece) = self.board[y][x] {
				// si elle apartient au joueur
				if piece.c == player {
					// on marque les cases qu'elle peut atteindre
					self.reachable_by_piece(x, y, map);
				}
			}
		}
	}
}
```

Créons une fonction `is_in_check` qui indique si un joueur donné est en échec.
```rust
fn is_in_check(self: &Game, player: Color) -> bool {
	let mut map = [[false; 8]; 8];
	let oponent = match player {Black=>White, White=>Black};
	// on marque les cases atteignables par l'oposant
	self.reachable_by_player(oponent, &mut map);
	for y in 0..8 {
		for x in 0..8 {
			// pour toute les pièces du plateau
			if let Some(piece) = self.board[y][x] {
				// si elle apartient au joueur, que c'est un Roi et qu'elle est
				// atteignable par l'oposant, alors le joueur est en échec
				if map[y][x] && piece.c == player && piece.t == King {
					return true;
				}
			}
		}
	}
	false
}
```

Créons une fonction `move_piece` qui déplace une pièce sur le plateau sans prendre en compte si c'est un cou autorisé.
```rust
fn move_piece(self: &mut Game, m: Move) -> Option<Piece> {
	// si il y a une pièce à l'emplacement d'origine, on la prend
	if let Some(piece) = self.board[m.src_y][m.src_x].take() {
		// et on la place à la destination, en retournant celle déjà présente
		// si il y en a une
		self.board[m.dst_y][m.dst_x].replace(piece)
	}
	else {
		None
	}
}
```
Les fonction `take` et `replace` sont des méthodes de `Option` définits dans la bibliothèque standard, n'hésite pas à consulter la documentation si elles te paraissent obscures.


Et enfin, créons une fonction `play_move` qui joue un cou en vérifiant qu'il est autorisé.
```rust
fn play_move(self: &mut Game, m: Move, player: Color) -> bool {
	if let Some(piece) = self.board[m.src_y][m.src_x] {
		// la pièce doit appartenir au joueur
		if piece.c == player {
			let mut map = [[false; 8]; 8];
			// la case doit être atteignable
			self.reachable_by_piece(m.src_x, m.src_y, &mut map);
			if map[m.dst_y][m.dst_x] {
				// on déplace la pièce
				self.move_piece(m);
				// on test si on est pas en échec
				if !self.is_in_check(player) {
					return true
				}
				else {
					// si on est en échec, il faut revenir en arrière
					return false;
				}
			}
		}
	}
	false
}
```
Le problème est que pour tester si le joueur est en échec, il faut d'abord jouer le cou. Donc il faut pouvoir revenir en arrière si necessaire. Le plus simple est tester le coup sur une copie du jeu.
```rust
fn play_move(self: &mut Game, m: Move, player: Color) -> bool {
	if let Some(piece) = self.board[m.src_y][m.src_x] {
		if piece.c == player {
			let mut map = [[false; 8]; 8];
			self.reachable_by_piece(m.src_x, m.src_y, &mut map);
			if map[m.dst_y][m.dst_x] {
				// on clone le jeu
				let mut projection = self.clone();
				projection.move_piece(m);
				if !projection.is_in_check(player) {
					// on applique le cou sur le jeu d'origine
					self.move_piece(m);
					return true
				}
			}
		}
	}
	false
}
```

## Progression

Retrouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/44/main.rs">`main.rs`</a> le projet.
