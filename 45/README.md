# Jeu d'échecs (7)

## Enfin, condons le `main`
```rust
fn main() {

}
```

Créons un jeu et affichons-le.
```rust
fn main() {
	let game = Game::new();
	game.print();
}
```
```
♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜ 
♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟ 
                
                
                
                
♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙ 
♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖ 
```
Parfait.

Maintenant on souhaite que le joueur Blanc entre un mouvement pour le jouer.
```rust
let game = Game::new();
game.print();
println!("White player");
let m = get_user_move_loop();
game.play_move(m, White); // ERROR
```
On a oublié de mettre `game` mutable. Et il faut vérifier si le coup était autorisé pour boucler tant que ce n'est pas le cas et demander au joueur blanc d'entrer un nouveau coup.
```rust
let mut game = Game::new();
game.print();
println!("White player");
loop {
    let m = get_user_move_loop();
    if game.play_move(m, White) {
        break
    }
    println!("this is not a valid move");
}
```
On doit faire la même chose pour le joueur Noir.
```rust
let mut game = Game::new();
game.print();
println!("White player");
loop {
    let m = get_user_move_loop();
    if game.play_move(m, White) {
        break
    }
    println!("this is not a valid move");
}
game.print();
println!("Black player");
loop {
    let m = get_user_move_loop();
    if game.play_move(m, Black) {
        break
    }
    println!("this is not a valid move");
}
```
On peut factoriser ce code en utilisant une boucle `for`.
```rust
let mut game = Game::new();
for &player in &[White, Black] {
    game.print();
    println!("{:?} player", player);
    loop {
        let m = get_user_move_loop();
        if game.play_move(m, player) {
            break
        }
        println!("this is not a valid move");
    }
}
```
On itère sur un tableau contenant les deux joueurs. Notez que le paterne du `for` est `&player`, de cette façon on déréférence, car souviens-toi qu'en itérant sur un `&[Color]` on récupère des `&Color` et non directement des `Color`.

Le problème est que le jeu ne demande qu'un seul coup pour chaqu'un des deux joueur avant de se terminer. Il faudrait boucler tant que le jeu n'est pas fini.

Il existe une méthode `cycle` sur les itérateurs qui les fait boucler indéfiniment, on pourrait l'utiliser sur notre tableau des deux joueurs.
```rust
for &player in (&[White, Black]).cycle() { // ERROR
    // ...
}
```
Le compilateur se pleins qu'il n'y a aucune méthode `cycle` pour le type `&[Color;2]`. Effectivement, le type `&[Color;2]` n'est pas un itérateur en soit (il n'implémente pas `Iterator`), en revanche, on peut obtenir un itérateur à partir de ce dernier. C'est pourquoi une boucle `for` l'accepte mais que l'on ne peut pas appeler `cycle` directemet dessus. Il faut convertir explicitement `&[Color; 2]` en itérateur pour pouvoir appeler `cycle` dessus.
```rust
for &player in (&[White, Black]).iter().cycle() {
    // ...
}
```
Il n'est pas necessaire de prendre l'adresse du tableau pour appeler `iter`, puisque cette dernière est une méthode, elle référence ou déréférence implicitement si necessaire.
```rust
for &player in [White, Black].iter().cycle() {
    // ...
}
```
Ce qui donne.
```rust
fn main() {
	let mut game = Game::new();
	for &player in [White, Black].iter().cycle() {
		game.print();
		println!("{:?} player", player);
		loop {
			let m = get_user_move_loop();
			if game.play_move(m, player) {
				break
			}
			println!("this is not a valid move");
		}
	}
}
```
Notre jeu d'échecs dans le terminal est déjà jouable. Mais il ne détecte pas sur un joueur est en échec et mat ou pat.

## Progression

Retrouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/45/main.rs">`main.rs`</a> le projet.
