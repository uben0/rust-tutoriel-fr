# Jeu d'échecs (4)

## Mouvement des pièces

Pour modéliser le mouvement des pièces, on va créer un itérateur.
```rust
#[derive(Clone, Copy)]
struct PosIter {
    x: usize,
    y: usize,
    dx: isize,
    dy: isize,
}
```
Les champs `x` et `y` contiennent les coordonés courantes. Les champs `dx` et `dy` contiennent la translation (delta x et delta y).

### Constructeur
```rust
fn new(x: usize, y: usize) -> PosIter {
    PosIter {x, y, dx: 0, dy: 0}
}
```
On initialise notre itérateur avec des coordonées de départ mais sans déplacement (`dx` et `dy` nuls).

### Modifier la translation
Ajoutons une méthode qui crée un nouvel itérateur à partir d'un déjà existant, en modifiant sa translation de `1` vers le nord.
```rust
fn north(self: &PosIter) -> PosIter {
    PosIter{
        x: self.x,
        y: self.y,
        dx: self.dx,
        dy: self.dy - 1,
    }
}
```
Ici, on récupère tout les champs en n'en modifiant qu'un seul. Trois champs sont copiés tel quel, hors il existe une syntax pour racourcir ce genre de situation.
```rust
fn north(self: &PosIter) -> PosIter {
    PosIter{ dy: self.dy - 1, .. *self }
}
```

On fait cela pour les quatres directions Nord, Sud, Ouest et Est.
```rust
fn north(self: &PosIter) -> PosIter {
    PosIter{dy: self.dy - 1, .. *self}
}
fn south(self: &PosIter) -> PosIter {
    PosIter{dy: self.dy + 1, .. *self}
}
fn west(self: &PosIter) -> PosIter {
    PosIter{dx: self.dx - 1, .. *self}
}
fn est(self: &PosIter) -> PosIter {
    PosIter{dx: self.dx + 1, .. *self}
}
```

### Plus de directions

Créons une méthode qui à partir d'un itérateur en renvoi quatres avec chacun une translation décalé de `1` vers un des axes.
```rust
fn axes(self: &PosIter) -> [PosIter; 4] {
    [
        self.north(),
        self.est(),
        self.south(),
        self.west(),
    ]
}
```
Créons une méthode qui à partir d'un itérateur en renvoi quatres avec chacun une translation décalé de `1` vers une des diagonales.
```rust
fn diagonals(self: &PosIter) -> [PosIter; 4] {
    [
        self.north().est(),
        self.est().south(),
        self.south().west(),
        self.west().north(),
    ]
}
```
Créons une méthode qui à partir d'un itérateur en renvoi quatres qui correspondent aux quatres symétries radiales (rotation à 90 degré).
```rust
fn radials(self: &PosIter) -> [PosIter; 4] {
    assert!(self.dx != 0 || self.dy != 0);
    [
        PosIter{dx:  self.dx, dy:  self.dy, .. *self},
        PosIter{dx: -self.dy, dy:  self.dx, .. *self},
        PosIter{dx: -self.dx, dy: -self.dy, .. *self},
        PosIter{dx:  self.dy, dy: -self.dx, .. *self},
    ]
}
```
Si cette dernière était appelé sur un itérateur avec une translation nule, les quatres itérateurs résultant serait identiques. On ne veut pas s'autoriser à l'utiliser ainsi, alors on place un `assert` qui stopera le programme si jamais cela se produisait.

## Implémentons `Iterator`

```rust
impl Iterator for PosIter {

    // l'iterateur retourne des coordonés
    type Item = (usize, usize);

    fn next(self: &mut PosIter) -> Option<(usize, usize)> {

        if self.dx == 0 && self.dy == 0 {
            // si la translation est nule, alors l'itérateur est dans
            // l'incapacité à produire de nouvelles positions
            return None
        }

        // on calcule la nouvelle position
        let res_x = self.x as isize + self.dx;
        let res_y = self.y as isize + self.dy;
        
        if (0..8).contains(&res_x) && (0..8).contains(&res_y) {
            // si elle est comprise en 0 et 8 exclu, c'est une
            // position valide

            // on l'enregistre
            self.x = res_x as usize;
            self.y = res_y as usize;
            // on l'a retourne
            Some((self.x, self.y))
        }
        else {
            // la nouvelle position est en dehors du plateau,
            // elle n'est donc pas valide
            None
        }
    }
}
```

## Exemples

### La Tour
Une tour en A1 peut se déplacer sur les cases:
```rust
let pos_iters: [PosIter; 4] = PosIter::new(0, 0).axes();

for i in 0..4 {
    for (x, y) in pos_iters[i] {
        println!("x={} y={}", x, y);
    }
}
```
```
x=1 y=0
x=2 y=0
x=3 y=0
x=4 y=0
x=5 y=0
x=6 y=0
x=7 y=0
x=0 y=1
x=0 y=2
x=0 y=3
x=0 y=4
x=0 y=5
x=0 y=6
x=0 y=7
```

### Le Fou
Un fou en D6 peut se déplacer sur les cases:
```rust
let pos_iters: [PosIter; 4] = PosIter::new(3, 5).diagonals();

for i in 0..4 {
    for (x, y) in pos_iters[i] {
        println!("x:{} y:{}", x, y);
    }
}
```
```
x:4 y:4
x:5 y:3
x:6 y:2
x:7 y:1
x:4 y:6
x:5 y:7
x:2 y:6
x:1 y:7
x:2 y:4
x:1 y:3
x:0 y:2
```

## Progression du projet
Trouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/42/main.rs">`main.rs`</a> le projet.
