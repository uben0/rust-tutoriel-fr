# Self

On a déjà vu dans un chapitre précédent comment déclarer des fonctions associés et des méthodes. Mais nous allons revenir sur les mots clés `Self` et `self`.

## Le type `Self`

Il est possible de désigner dans un block `impl` le type sur lequel s'applique l'implémentation avec le mot clé `Self`. Ce mot clé s'avère necessaire dans des situations où ce type n'est pas déterminé. Mais il est fortement conseillé de l'utiliser dès que possible, même quand son utilisation n'est pas obligatoire.

```rust
struct Hello{
    x: usize,
}

impl Hello {
    fn new() -> Hello {
        return Hello{ x: 0 }
    }
    fn increment(self: &mut Hello) {
        self.x += 1;
    }
}
```
En remplaçant par `Self`, cela donne:
```rust
impl Hello {
    fn new() -> Self {
        return Self{ x: 0 }
    }
    fn increment(self: &mut Self) {
        self.x += 1;
    }
}
```
De cette façon, le type `Hello` n'apparait qu'une seul fois (`impl Hello`). C'est une bonne habitude à prendre.

## Mot clé `self`

Le mot clé `self` est différent, il désigne une variable lorsqu'on définit une méthode. Cette variable représente la valeur sur laquelle s'applique la méthode.
```rust
impl Hello {
    fn increment(self: &mut Self) {
        self.x += 1;
    }
}
```
De cette façon, on peut appeler `increment` comme une méthode de `Hello`.
```rust
let mut hello = Hello::new();
hello.increment();
```
Ce qui est équivalent à:
```rust
let mut hello = Hello::new();
Hello::increment(&mut hello);
```
Tu remarqueras que dans le cas de la notation avec le `.` nous n'avons pas eu besoins d'emprunter explicitement `hello`. Si on devait explicitement l'emprunter, cela donnerait:
```rust
(&mut hello).increment();
```

### Contraction
Il est possible de contracter la notation avec `self`.
```rust
impl Hello {
    fn increment(&mut self) {
        self.x += 1;
    }
}
```
| long | court |
|--|---------|
|`self: Self` | `self` |
|`self: &Self` | `&self` |
|`self: &mut Self` | `&mut self` |

Et oui, quand on est programmeur on est fainéant, si on peut gagner quelques charactères, c'est toujours bon à prendre (et puis ça améliore la lisibilité).

Même ci les deux notaions sont équivalentes, il est fortement recomandé de n'utiliser que la notation contractée.