# Les closures

Il est possible de déclarer une variable qui stocke une fonction (ou plutôt qui réfère une fonction).

```rust
fn g(x: i32) -> i32 {
    x * 2 + 3
}

fn main() {
    let f: fn(i32)->i32 = g;
    println!("f(3) = {}", f(3));
}
```
Le type de `f` est `fn(i32)->i32`, et oui, c'est un type dont je n'avais pas encore parlé. Le type `fn` est le type d'une fonction.

## Une closure

Il est possible de déclarer une fonction sous forme de valeur. On appelle ça une closure.
```rust
let f = |x: i32| x * 2 + 3;
println!("f(3) = {}", f(3));
```

## Les traits `Fn`, `FnMut` et `FnOnce`

Il serait pratique de pouvoir accepter des fonctions comme paramètre de fonctions, comme pour la méthode `map` d'un itérateur qui utilise la fonction qui lui est passé pour transformer ses éléments.
```rust
for i in [1, 2, 3].iter().map(|x| 2 * x) {
    println!("{}", i);
}
```
```
2
4
6
```
On peut imaginer créer notre propre fonction accèptant en paramètre un fonction prenant un `i32` et renvoyant un `i32`.
```rust
fn test_function(f: fn(i32)->i32) {
    for i in 0..10 {
        println!("f({}) = {}", i, f(i));
    }
}
```
Testons-la !
```rust
test_function(|x| x + 10);
```
```
f(0) = 10
f(1) = 11
f(2) = 12
f(3) = 13
f(4) = 14
f(5) = 15
f(6) = 16
f(7) = 17
f(8) = 18
f(9) = 19
```
Magnifique !

### Cependant...

Imaginons que l'on souhaite utiliser une variable tiers.
```rust
let n: i32 = user_input();
test_function(|x| x + n); // ERROR
```
Le compilateur se plein que les types ne correspondent pas, que le type attendu est un poiteur de fonction (`fn`) mais que le type passé est une closure.

Une closure sans environement (qui ne capture pas de variable) est équivalente à une fonction classique. Mais ce n'est plus le cas lorsqu'elle utilse (capture) des variables de leur environement (hors c'est souvent très utile), car elle doit respecter leur duré de vie. Son type devient opaque. Cependant, les closures implémentent un des traits `Fn`, `FnMut` ou `FnOnce`. Je ne vais pas expliquer la différence entre ces trois traits mais tu peux consulter la documentation pour plus d'informations.

Ces traits permettent d'écrire du code générique acceptant les closures.
```rust
fn test_function<F: Fn(i32)->i32>(f: F) {
    for i in 0..10 {
        println!("f({}) = {}", i, f(i));
    }
}
```
```rust
let n: i32 = user_input();
test_function(|x| x + n);
```

## Conclusion
Tu ne seras probablement pas amené à écrire des fonctions acceptant des closures mais tu seras fréquement amené à en utiliser comme pour la méthode `map` d'un itérateur qui s'avère très utile.
