# Jeu d'échecs (5)

On va créer une fonction qui indique pour une pièce du plateau, quelles cases elle peut atteindre. Ce sera une méthode de `Game`.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    unimplemented!()
    // cette macro permet d'indiquer un endroit du code qui
    // n'a pas encore était implémenté
}
```
Elle prend en paramètre les coordonnées de la pièce sélectionnée et un tableau à deux dimensions de booléens représentant les cases où la pièce peut se rendre.

Cette fonction a pour but de mettre à `true` les cases atteignables, elle ne met pas à `false` celles qui ne le sont pas, car on souhaite par la suite appeler plusieur fois cette fonction sur un même plateau de booléens initialisés à `false`.

On test s'il y a bien une pièce à l'emplacement, si oui, on la récupère. Et on match en fonction de son type.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Rook   => unimplemented!(),
            Knight => unimplemented!(),
            Bishop => unimplemented!(),
            Queen  => unimplemented!(),
            King   => unimplemented!(),
        }
    }
}
```
On crée une méthode de `Game` qui pour un itérateur de position (`PosIter`) et une couleur de joueur (`Color`), remplit dans le tableau de booléens les cases accessibles.
```rust
fn reachable_by_pos_iter(
    self: &Game,
    pos_iter: PosIter,
    player: Color,
    map: &mut [[bool; 8]; 8]
) {
    for (x, y) in pos_iter {
        if let Some(p) = self.board[y][x] {
            if p.c != player {
                // si il y a une pièce de l'adversaire
                // alors on peut la manger
                map[y][x] = true;
            }
            // si il y a une pièce (noire ou blanche)
            // alors on ne pourra pas aller plus loins
            break
        }
        // si il n'y a pas de pièce, on peut s'y déplacer
        map[y][x] = true;
        // et on continu d'avancer sur la trajectoire
    }
}
```

On reviens à notre fonction `reachable_by_piece` et génère les `4` itérateurs de positions de la Tour. Pour chacun d'eux on appelle `reachable_by_pos_iter`.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => unimplemented!(),
            Bishop => unimplemented!(),
            Queen  => unimplemented!(),
            King   => unimplemented!(),
            Rook => {
				let pos_iters = PosIter::new(x, y).axes();
				self.reachable_by_pos_iter(pos_iters[0], piece.c, map);
				self.reachable_by_pos_iter(pos_iters[1], piece.c, map);
				self.reachable_by_pos_iter(pos_iters[2], piece.c, map);
				self.reachable_by_pos_iter(pos_iters[3], piece.c, map);
            }
        }
    }
}
```
Ce n'est pas très malin de faire un appel sur chaque éléments du tableau. On peut utiliser un boucle for pour factoriser le code.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => unimplemented!(),
            Bishop => unimplemented!(),
            Queen  => unimplemented!(),
            King   => unimplemented!(),
            Rook => {
                for pos_iter in PosIter::new(x, y).axes() {
                    //          ^^^^^^^^^^^^^^^^^^^^^^^^^ERROR
                    self.reachable_by_pos_iter(pos_iter, piece.c, map)
                }
            }
        }
    }
}
```
(Cette erreur n'aura pas lieu avec les dernière mise à jour *Rust*, mais faisont comme si l'erreur se produisait.)

La méthode `axes` renvoi un tableau de type `[PosIter; 4]`, or on ne peut pas utiliser directement un tableau comme itérateur. En revanche, une référence sur un tableau fait l'affaire. Il suffit d'emprunter le tableau avec `&`.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => unimplemented!(),
            Bishop => unimplemented!(),
            Queen  => unimplemented!(),
            King   => unimplemented!(),
            Rook => {
                for pos_iter in &PosIter::new(x, y).axes() {
                    self.reachable_by_pos_iter(pos_iter, piece.c, map)
                    //                         ^^^^^^^^ERROR
                }
            }
        }
    }
}
```
Le compilateur n'est toujours pas content, il se plein que le type attendu est `PosIter` mais que le type passé est `&PosIter`. En effet, en itérant sur un type `&[PosIter; 4]`, les valeurs retournées par l'itérateur son de type `&PosIter`. Pour régler le problème, il faut déréférer `pos_iter` à l'aide de l'opérateur `*`.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => unimplemented!(),
            Bishop => unimplemented!(),
            Queen  => unimplemented!(),
            King   => unimplemented!(),
            Rook => {
                for pos_iter in &PosIter::new(x, y).axes() {
                    self.reachable_by_pos_iter(*pos_iter, piece.c, map)
                }
            }
        }
    }
}
```
On fait la même chose pour le Fou et la Reine.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => unimplemented!(),
            King   => unimplemented!(),
            Rook => {
                for pos_iter in &PosIter::new(x, y).axes() {
                    self.reachable_by_pos_iter(*pos_iter, piece.c, map)
                }
            }
            Bishop => {
                for pos_iter in &PosIter::new(x, y).diagonals() {
                    self.reachable_by_pos_iter(*pos_iter, piece.c, map)
                }
            }
            Queen => {
                for pos_iter in &PosIter::new(x, y).axes() {
                    self.reachable_by_pos_iter(*pos_iter, piece.c, map)
                }
                for pos_iter in &PosIter::new(x, y).diagonals() {
                    self.reachable_by_pos_iter(*pos_iter, piece.c, map)
                }
            }
        }
    }
}
```
Maintenant on aimerait s'occuper du Roi et on aimerait récupérer ce qu'on a fait pour la Reine, car après tout, le roi se déplace comme la reine mais est limité à une case de distance. Hors, il y a la méthode `take` sur les itérateurs qui permet de les limiter.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => unimplemented!(),
            King => {
                for pos_iter in &PosIter::new(x, y).axes() {
                    self.reachable_by_pos_iter(*pos_iter.take(1), piece.c, map)
                    //                         ^^^^^^^^^^^^^^^^^ERROR
                }
                for pos_iter in &PosIter::new(x, y).diagonals() {
                    self.reachable_by_pos_iter(*pos_iter.take(1), piece.c, map)
                    //                         ^^^^^^^^^^^^^^^^^ERROR
                }
            }
            // on a déjà implémenté ceux qui suivent mais pour
            // éviter que le code soit trop long, je retire
            // ce qui ne concerne pas les explications
            Rook => unimplemented!(),
            Bishop => unimplemented!(),
            Queen => unimplemented!(),
        }
    }
}
```
Il n'y a plus besoins de déréférencer `pos_iter` car l'opérateur `.` le fait automatiquement.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => unimplemented!(),
            King => {
                for pos_iter in &PosIter::new(x, y).axes() {
                    self.reachable_by_pos_iter(pos_iter.take(1), piece.c, map)
                    //                         ^^^^^^^^^^^^^^^^ERROR
                }
                for pos_iter in &PosIter::new(x, y).diagonals() {
                    self.reachable_by_pos_iter(pos_iter.take(1), piece.c, map)
                    //                         ^^^^^^^^^^^^^^^^ERROR
                }
            }
            Rook => unimplemented!(),
            Bishop => unimplemented!(),
            Queen => unimplemented!(),
        }
    }
}
```
Le compilateur n'est toujours pas content, il se plein que le type attendu est `PosIter` mais que celui passé est `Take<PosIter>`. En effet, la méthode `take` d'un itérateur fonctionne en envelopant l'itérateur dans un autre itérateur. On va changer le prototype de la fonction `reachable_by_pos_iter` de sorte qu'elle soit générique sur tout itérateur de position `(usize, usize)`.
```rust
fn reachable_by_pos_iter<
    T: Iterator< Item=(usize, usize) >
>(
    self    : &Game,
    pos_iter: T,           // on remplace `PosIter` par `T`
    player  : Color,
    map     : &mut [[bool; 8]; 8]
) {
    for (x, y) in pos_iter {
        if let Some(p) = self.board[y][x] {
            if p.c != player {
                map[y][x] = true;
            }
            break
        }
        else {
            map[y][x] = true;
        }
    }
}
```
Maintenant `reachable_by_pos_iter` accèpte le type `Take<PosIter>`. Cependant, la contrainte ne fait que garantir que l'itèrateur génère des `(usize, usize)` mais pas que ce soit des positions valides, on n'a pas de garantie que les deux entiers soit compris entre `0` et `8` exclu. L'idéal serait de créer un type `Coords` garantissant la validité des coordonnés, mais on ne s'embetera pas avec ça.

On peut maintenant s'attaquer au déplacements du cavalier.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn   => unimplemented!(),
            Knight => {
				for pos_iter in &PosIter::new(x, y)
					.north().north().est().radials()
				{
					self.reachable_by_pos_iter(pos_iter.take(1), piece.c, map)
				}
				for pos_iter in &PosIter::new(x, y)
					.north().north().west().radials()
				{
					self.reachable_by_pos_iter(pos_iter.take(1), piece.c, map)
				}
			}
            King => unimplemented!(),
            Rook => unimplemented!(),
            Bishop => unimplemented!(),
            Queen => unimplemented!(),
        }
    }
}
```
On crée deux mouvements Nord Nord Est et Nord Nord Ouest et on génère les quatres symétries radiales (rotations à 90 degrés). On limite à `1` le nombre d'itération.

Et pour finir, on s'attaque aux déplacements du pion. On crée deux variables qui dépendent de la couleur du pion, un itérateur de position orienté soit vers le Nord, soit vers le Sud et la ligne de pépart.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn => {
				let (pos_iter, start) = match p.c {
					Black => {(PosIter::new(x, y).south(), 1)}
					White => {(PosIter::new(x, y).north(), 6)}
				};
				unimplemented!()
			}
            Knight => unimplemented!(),
            King => unimplemented!(),
            Rook => unimplemented!(),
            Bishop => unimplemented!(),
            Queen => unimplemented!(),
        }
    }
}
```

On ne pourra pas utiliser `reachable_by_pos_iter` car le Pion se comporte différemment des autres pièces, il ne peut pas manger par l'avant, et ne peut aller sur les cotés que en mangeant.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn => {
				let (pos_iter, start) = match p.c {
					Black => {(PosIter::new(x, y).south(), 1)}
					White => {(PosIter::new(x, y).north(), 6)}
				};
				for (x, y) in pos_iter.take(1 + (y == start) as usize) {
					if self.board[y][x].is_some() {
						break;
					}
					map[y][x] = true;
				}
				unimplemented!()
			}
            Knight => unimplemented!(),
            King => unimplemented!(),
            Rook => unimplemented!(),
            Bishop => unimplemented!(),
            Queen => unimplemented!(),
        }
    }
}
```
On limite à `1` itération sauf si la pièce est sur la ligne de départ, dans ce cas la limite est à `2` (on converti un `bool` en `usize`, ce qui donne `false`=>`0` et `true`=>`1`). On s'assure qu'il n'y est pas de pièces car le pion ne peu pas manger par l'avant.

Maintenant on test les déplacements an biais du pion.
```rust
fn reachable_by_piece(
    self: &Game,
    x: usize, y: usize,
    map: &mut [[bool; 8]; 8]
) {
    if let Some(piece) = self.board[y][x] {
        match piece.t {
            Pawn => {
				let (pos_iter, start) = match p.c {
					Black => {(PosIter::new(x, y).south(), 1)}
					White => {(PosIter::new(x, y).north(), 6)}
				};
				for (x, y) in pos_iter.take(1 + (y == start) as usize) {
					if self.board[y][x].is_some() {
						break;
					}
					map[y][x] = true;
				}
				if let Some((x, y)) = pos_iter.est().next() {
					if let Some(dst_piece) = self.board[y][x] {
						if dst_piece.c != piece.c {
							map[y][x] = true;
						}
					}
				}
				if let Some((x, y)) = pos_iter.west().next() {
					if let Some(dst_piece) = self.board[y][x] {
						if dst_piece.c != piece.c {
							map[y][x] = true;
						}
					}
				}
			}
            Knight => unimplemented!(),
            King => unimplemented!(),
            Rook => unimplemented!(),
            Bishop => unimplemented!(),
            Queen => unimplemented!(),
        }
    }
}
```

## Progression

Retrouves dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/43/main.rs">`main.rs`</a> le projet.
