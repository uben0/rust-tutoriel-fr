CHAPTERS_DIR = $(shell ls | grep -o '[0-9][0-9]')
CHAPTERS = $(addsuffix .html,$(addprefix target/c, $(CHAPTERS_DIR)))

all: target/style.css target/index.html $(CHAPTERS)

target/index.html: $(CHAPTERS) begin.txt end.txt gen-index.sh target
	./gen-index.sh

target/c%.html: %/README.md begin.txt end.txt gen-chapter.sh target
	./gen-chapter.sh $(shell echo $< | grep -o '[0-9][0-9]')

target/style.css: style.css target
	cp $< $@

target:
	mkdir target

clean:
	rm -rf target

.PHONY: all clean