# Boucle sans fin

Écrivons une fonction qui récupère une saisie de l'utilisateur comme un entier positif.
```rust
fn user_intput_u32() -> u32 {
    println!("Rentrez un entier positif :");
    let a: u32 = user_input();
    return a;
}
```

Mais on aimerais que cette fonction redemande à l'utilisateur d'entrer un entier tant qu'il n'en a pas rentré un qui soit pair.

```rust
fn user_intput_u32() -> u32 {
    while true {
        println!("Rentrez un entier positif :");
        let a: u32 = user_input();
        if a % 2 == 0 {
            return a;
        }
    }
    // ERROR
}
```
Le compilateur se plein. Il dit qu'une valeur de type `u32` est attendu mais qu'une valeur de type `()` est fourni. Souviens-toi que tout block a une valeur, et le dernier élément d'un block devient sa valeur. Ici, le dernier élément du block de la fonction est la boucle `while` qui ne peut avoir comme valeur que `()`. Mais le block de la fonction doit avoir une valeur de type `u32`. On peut régler le problème en plaçant un `0` à la fin.
```rust
fn user_intput_u32() -> u32 {
    while true {
        println!("Rentrez un entier positif :");
        let a: u32 = user_input();
        if a % 2 == 0 {
            return a;
        }
    }
    0
}
```

## Boucle `loop`

On peut régler le problème d'une façon plus élégante en utilisant une boucle `loop`.
```rust
fn user_intput_u32() -> u32 {
    loop {
        println!("Rentrez un entier positif :");
        let a: u32 = user_input();
        if a % 2 == 0 {
            return a;
        }
    }
}
```

La boucle `loop` a une valeur particulière appelé `never`, son type est `!`. Oui, oui, c'est peu commun.

## Type never `!`

Le type `never` est définit dans la bibliothèque standard. Mais on peu en définir un équivalent nous même.
```rust
enum Jamais {
}
```
Voilà, le type `Jamais` est une énumération avec `0` variante. Une valeur de type `Jamais` ne peut pas exister. Essaye de créer une valeur de type `Jamais`. Eh bien le type `never` c'est pareil. Il sert à représenter une valeur qui n'existe pas.

Par exemple, même l'instruction `return` a une valeur (quand je te disais que absolument tout avait une valeur). Mais cette dernière est de type `never`.
```rust
fn lol() -> u32 {
    let jamais: ! = {
        return 42
    };
}
```
Effectivement, la variable `jamais` ne recevra jamais de valeur puisque la fonction retournera avant à cause du `return`.

## Réconsilier

On peut toujours affecter une valeur de type `never` à un emplacement de n'importe quel autre type, puisqu'une telle valeur est garantie de ne pas exister, on a pas besoins de se soucier que les types correspondent. Malin.
```rust
fn lol() -> u32 {
    let jamais: char = {
        return 42
    };
}
```
On a mis `char` mais on aurait pu mettre n'importe quel autre type alambiqué et ce code serait toujours valide.
