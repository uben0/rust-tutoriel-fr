#!/bin/bash
(
	cat begin.txt;
	echo '<h1>Tutoriel Rust</h1>';
	echo "<ul class=\"chapters\">";
	for f in [0-9][0-9]; do
		echo "<a id=\"c$(echo $f | grep -o '[0-9][0-9]')\" href=\"c";
		echo $f;
		echo ".html\"><li class=\"chapter\">";
		cat $f/README.md | sed -n '1p' | sed 's/^..//';
		echo "</li></a>";
	done
	echo "</ul>";
) > target/index.html
