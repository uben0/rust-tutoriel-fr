# Slices

Imaginons une fonction affichant le contenu d'un tableau de `i32`:
```rust
fn affiche_tableau_i32(tableau: &[i32; 10]) {
	for i in 0..10 {
		println!("{} -> {}", i, tableau[i]);
	}
}
```
Le type de `tableau` est `&[i32; 10]`, ce qui signifie que c'est une référence sur un tableau de 10 entiers, mais ce n'est pas pratique puisque ça signifie qu'on ne peut lui passer que des tableaus de taille `10`. Heuresement il y a une solution:
```rust
fn affiche_tableau_i32(tableau: &[i32]) {
	for i in 0..tableau.len() {
		println!("{} -> {}", i, tableau[i]);
	}
}
```

Le type `[i32]` est un tableau de `i32` dont la taille n'est pas connu. Il n'est pas possible de déclarer une variable dont le type n'a pas une taille connu, car une variable vit sur la pile et pour ça, sa taille doit être connu.
```rust
let t: [i32] = [3, 4, 5][..]; // ERROR
```
(la notation `[..]` permet ici de convertir `[i32; 3]` en `[i32]`, on reviendra après sur cette notation)

En revanche, une référence (étant une adresse mémoire) a une taille connu, et rien n'oblige une valeur pointée par une référence à avoir une taille connu.
```rust
let t: &[i32] = &[3, 4, 5][..]; // OK
```

## Pointeur lourd
Pour mieux comprendre ce qui se passe, imaginons la fonction `affiche_tableau_i32` en C.

```cpp
void affiche_tableau_int(int* tableau, size_t taille) {
	for (size_t i = 0; i < taille; i++) {
		printf("%d -> %d", i, tableau[i]);
	}
}
```
En C, on a besoins d'un pointeur au début du tableau, et de sa taille.

En Rust, c'est exactement ce que contient `&[i32]`, l'adresse mémoire qui pointe au début du tableau et sa taille, c'est ce qu'on appelle un pointeur lourd. On dit d'une référence que c'est un pointeur lourd lorsqu'elle contient plus qu'une adresse mémoire. On verra qu'il y a d'autres cas où des références sont en fait des pointeurs lourds.
```
poiteur lourd         tableau d'entiers en mémoire
▄┏━━━━━━━━━┓          ┌───┬───┬───┬───┬───┬───┐
█┃ 0x7f004 ┣━╺╺╺╺╺╺╺╺◆│ 8 │ 0 │ 6 │ 2 │ 9 │ 3 │
█┡━━━━━━━━━┩          └───┴───┴───┴───┴───┴───┘
█│       6 │ taille
▀└─────────┘
```

On appelle notre fonction de la façon suivante:
```rust
let tableau: [i32; 6] = [3, 6, 4, 1, 0, 7];
affiche_tableau(&tableau);
```
On lui passe le type `&[i32; 6]` et la conversion est automatique vers le type `&[i32]`, pas besoins de la notation `[..]`. C'est un très rare cas de conversion implicite.

Un tableau dont la taille n'est pas connu est appelé une `slice` (tranche).

## Sélectionner

On a vu la notation `[..]`, cette dernière permet de sélectionner tout le tableau. Mais on peu par exemple sélectionner les `10` premiers éléments avec `[..10]`, ou encore tous le éléments sauf les `3` premiers avec `[3..]`.

```rust
let t = [6, 1, 0, 4, 6, 2, 8, 3];

println!("{:?}", &t[..]);
println!("{:?}", &t[..5]);
println!("{:?}", &t[5..]);
println!("{:?}", &t[3..7]);

let n = 4;
println!("{:?}", &t[..n]);
```

```
[6, 1, 0, 4, 6, 2, 8, 3]
[6, 1, 0, 4, 6]
[2, 8, 3]
[4, 6, 2, 8]
[6, 1, 0, 4]
```
