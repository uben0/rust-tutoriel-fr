fn user_input<T: std::str::FromStr>() -> T
where <T as std::str::FromStr>::Err: std::fmt::Debug
{
	let mut l = String::new();
	std::io::stdin().read_line(&mut l).unwrap();
	T::from_str(l.as_str().trim()).unwrap()
}

fn main() {
	// on récupère un entier aléatoir
	let random: u32 = rand::random();
	// on le ramène sur l'interval de 0 à 99
	let mystere: u32 = random % 100;

	println!("Le nombre mystère a été choisi. Devine-le !");
	println!("");

	let mut essais: u32 = 10;

	while essais > 0 {

		println!("Ta réponse : ");
		let reponse: u32 = user_input();
		
		if mystere < reponse {
			println!("Le nombre est plus petit que {}.", reponse);
			println!("");
		}
		else if mystere > reponse {
			println!("Le nombre est plus grand que {}.", reponse);
			println!("");
		}
		else {
			// on ne test pas si mystere == reponse car si il n'est pas plus
			// grand, ni plus petit, alors il est égal
			println!("Bravo, tu as deviné le nombre mystère !");
			break
		}

		essais = essais - 1;

		println!("Il te reste {} essais.", essais);
	}

	if essais == 0 {
		println!("Désolé, tu as utilisé tes 10 essais.");
		println!("Le nombre mystère était {}.",	mystere);
	}
}
