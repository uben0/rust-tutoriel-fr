# Les structures

Il existe beaucoup de types, on a vu les entiers, les décimaux, les booléens, les tableaux et même vu brièvement les tuples.

On va voir maintenant les structures, on va définir une structure `Point` représentant un point sur un plan en deux dimensions:

```rust
struct Point {
	x: f64,
	y: f64,
}
```
On crée une variable de type `Point` de la façon suivante :
```rust
let p: Point = Point { x: 8.5, y: 1.0 };
```
On peut modifier un atribut de `Point` de la façon suivante :
```rust
let p: Point = Point { x: 8.5, y: 1.0 };
p.x = 6.0; // ERROR
```
Il y a une erreur, effectivement, `p` n'est pas mutable, il faut corriger ça:
```rust
let mut p: Point = Point { x: 8.5, y: 1.0 };
p.x = 6.0; // OK
```

## Afficher une structure
On peut afficher notre structure `Point` de la sorte:
```rust
println!("{:?}", p); // ERROR
```
Pour pouvoir afficher une structure en mode débuggage, il faut ajouter devant sa déclaration la ligne `#[derive(Debug)]`, ce qui donne:
```rust
#[derive(Debug)]
struct Point {
	x: f64,
	y: f64,
}

fn main() {
	let p: Point = Point { x: 8.5, y: 1.0 };
	println!("{:?}", p); // OK
}
```
Pour l'instant, la ligne `#[derive(Debug)]` parait obscur, mais je l'expliquerai plus tard.

On peut imaginer une structure `Cercle` utilisant `Point`:
```rust
#[derive(Debug)]
struct Cercle {
	centre: Point,
	rayon: f64,
}
```
On peut créer une variable de type `Cercle` de la sorte:
```rust
let p: Point = Point { x: 10.0, y: 7.0 };
let c: Cercle = Cercle { centre: p, rayon: 2.0 };
```

Affichons `c`.
```rust
println!("{:?}", c);
```
```
Cercle { centre: Point { x: 10.0, y: 7.0 }, rayon: 2.0 }
```
C'est difficilement lisible, on peut remplacer `{:?}` par `{:#?}`:
```rust
println!("{:#?}", c);
```
```
Cercle {
    centre: Point {
        x: 10.0,
        y: 7.0,
    },
    rayon: 2.0,
}
```
C'est beaucoup mieux. En rajoutant `#` on indique que l'on souhaite un affichage sur plusieurs lignes indentées.