# Tri de tableau
Coder un algorithme de tri est une bonne façon de se familiariser avec un langage de programmation.


## Valeurs aléatoires

Pour remplir notre tableau de valeurs aléatoires, on doit dans un premier temps le déclarer avec une valeur initiale, puis itérer sur chaque emplacement pour y affecter une valeur aléatoire :
```rust
let mut tableau: [u32; 10] = [0; 10];
for i in 0..10 {
	let aleatoir: u32 = rand::random();
	tableau[i] = aleatoir % 100;
}
```
Notez l'utilisation du mot clé `mut` pour `tableau` étant donné qu'il est modifié dans la boucle.

## Algorithme de tri

Maintenant, écrit un algorithme de tri de tableau, tu as tous les outils en mains. Tu peux trouver un algorihtme par toi même, mais si tu as du mal, je te conseil le tri par sélection (<a class="link" href="https://fr.wikipedia.org/wiki/Tri_par_s%C3%A9lection">lien wikipedia: tri sélection</a>).

Tu peut échanger deux valeurs d'un tableau de cette façon :
```rust
let mut tableau = [7, 8, 3, 6];
// on veut échanger le 7 et le 3
let temporaire = tableau[0];
tableau[0] = tableau[2];
tableau[2] = temporaire;
```

# Correction
Une correction se trouve dans le fichier <a href="https://gitlab.com/uben0/rust-tutoriel-fr/-/raw/master/15/correction.rs">`correction.rs`</a>.
